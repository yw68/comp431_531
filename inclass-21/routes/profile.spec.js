const expect = require('chai').expect
const fetch = require('isomorphic-fetch')

const url = (path) => `http://127.0.0.1:3000${path}`


describe('Validate Profile functionality', () => {

    it('should update headline', (done) => {
        let preHeadline;
        let latHeadline = "new headline"
        fetch(url("/headlines"), {
            "method" : 'GET',
            "headers": {'Content-Type' : 'application/json' }
        })
            .then(res => {
                expect(res.status).to.eql(200)
                return res.json()
            })
            .then(res => {
                preHeadline = res.headlines
            })

            .then(_=>{

                return fetch(url("/headline"), {
                    "headers": {'Content-Type': 'application/json'},
                    "method": 'PUT',
                    "body": JSON.stringify({accountName: 'yw68',headline: latHeadline})
                })
            })
            .then(res=>{
                expect(res.status).to.equal(200)
                return res.json()
            })
            .then((res)=>{
                expect(res.headline).to.equal(latHeadline)
            })

            .then(_=>{
                return fetch(url("/headlines/" + 'yw68'), {
                    "method": 'GET',
                    "headers": {'Content-Type': 'application/json'}
                })
            })
            .then(res => {
                expect(res.status).to.equal(200)
                return res.json()
            })
            .then(res => {
                expect(res.headlines).to.equal(latHeadline)
                expect(res.headlines).to.not.equal(preHeadline)
            })
            .then(done)
            .catch(done)
    }, 200)

    it('should update email or zip', (done) => {
        let preEmail;
        let latEmail = "new@test.com"
        fetch(url("/email"), {
            "method" : 'GET',
            "headers": {'Content-Type' : 'application/json' }
        })
            .then(res => {
                expect(res.status).to.eql(200)
                return res.json()
            })
            .then(res => {
                preEmail = res.email
            })

            .then(_=>{
                return fetch(url("/email"), {
                    "headers": {'Content-Type': 'application/json'},
                    "method": 'PUT',
                    "body": JSON.stringify({accountName: 'yw68',email: latEmail})
                })
            })
            .then(res=>{
                expect(res.status).to.equal(200)
                return res.json()
            })
            .then((res)=>{
                expect(res.email).to.equal(latEmail)
            })

            .then(_=>{
                return fetch(url("/email" + 'yw68'), {
                    "method": 'GET',
                    "headers": {'Content-Type': 'application/json'}
                })
            })
            .then(res => {
                expect(res.status).to.equal(200)
                return res.json()
            })
            .then(res => {
                expect(res.email).to.equal(latEmail)
                expect(res.email).to.not.equal(preEmail)
            })
            .then(done)
            .catch(done)
    }, 200)

    it('content (headline, profile info, articles, etc) is only viewable by logged in users', (done) => {
        fetch(url("/email"), {
            "method" : 'GET',
            "headers": {'Content-Type' : 'application/json' },
            "cookies": {'sid': 'fake'}
        })
            .then(res => {
                expect(res.status).to.eql(401)
            })
            .then(done)
            .catch(done)
    }, 200)

    it('must login again to view content', (done) => {
        fetch(url("/login"), {
            "method" : 'PUT',
            "headers": {'Content-Type' : 'application/json' },
            "accountName": 'yw69',
            "psw": 'yw69'
        })
            .then(res => {
                expect(res.status).to.eql(200)
            })
            .then(_=>{
                return fetch(url("/headlines/" + 'yw68'), {
                    "method": 'GET',
                    "headers": {'Content-Type': 'application/json'}
                })
            })
            .then(res => {
                expect(res.status).to.equal(200)
                return res.json()
            })
            .then(done)
            .catch(done)
    }, 200)

})