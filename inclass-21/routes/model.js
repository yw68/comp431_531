var mongoose = require('mongoose')
var bcrypt = require('bcryptjs');

var commentSchema = new mongoose.Schema({
    author: String,
    date: Date,
    text: String
})

var postSchema = new mongoose.Schema({
    author: String,
    img: String,
    date: Date,
    text: String,
    comments: [ commentSchema ]
})

var userSchema = new mongoose.Schema({
    accountName: String,
    displayName: String,
    zip: Number,
    dob: Date,
    following: [ String ],
    avatar: String,
    email: String,
    phoneNumber: String,
    headline: String,
    psw: String
})


exports.Post = mongoose.model('posts', postSchema)
exports.User = mongoose.model('users', userSchema)
exports.Comment = mongoose.model('comments', commentSchema)
//exports.Profile = mongoose.model('profiles', profileSchema)

module.exports.createUser = function(newUser, callback){
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newUser.psw, salt, function(err, hash) {
            newUser.psw = hash;
            newUser.save(callback);
            console.log(bcrypt.compareSync(newUser.psw, password))
        });
    });
}

module.exports.createPost = function(newPost, callback){
    newPost.save(callback);
}
