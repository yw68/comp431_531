const express = require('express');
const router = express.Router();
const User = require('./model').User;
const cookieKey = 'sid';
const redis = require("redis"),
    client = redis.createClient();

const isLoggedIn = (req, res, next) => {
    const sid = req.cookies[cookieKey];
    if (!sid) {
        return res.sendStatus(401)
    }
    client.get(sid, function (err, username) {
        if (err) throw err;
        if (username) {
            next()
        } else {
            res.sendStatus(401)
        }
    })
};

/* GET users listing. */
router.get('/:user?', function(req, res, next) {
    const user = req.params.user;
    if(!user) {
        let user;
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        });
        User.findOne({accountName: user}, function (err, u) {
            res.status(200).send({following: u.following})
        })
    }else {
        User.findOne({accountName: user}, function (err, user) {
            res.status(200).send({following: user.following})
        });
    }
});

router.put('/', function(req, res, next) {
    let user = req.body.accountName;
    let following = [];
    User.findOne({accountName: user}, function (err, u) {
        following = u.following;
        User.findOneAndUpdate(
            {accountName: req.body.accountName},
            {$set: {following: following.push(req.body.follower)}},
            {upsert: false},
            function (err, newUser) {
                if (err) res.send(err);
                else {
                    console.log(newUser);
                    res.status(200).send({following: newUser.following});
                }
            }
        )
    })
});

router.delete('/', function(req, res, next) {
    let user = req.body.accountName;
    let following = [];
    User.findOne({accountName: user}, function (err, u) {
        following = u.following.filter(e => e !== req.body.follower);
        User.findOneAndUpdate(
            {accountName: req.body.accountName},
            {$set: {following: following}},
            {upsert: false},
            function (err, newUser) {
                if (err) res.send(err);
                else {
                    console.log(newUser);
                    res.status(200).send({following: newUser.following});
                }
            }
        )
    })
});

module.exports = router;