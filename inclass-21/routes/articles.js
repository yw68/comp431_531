const express = require('express');
const router = express.Router();
const model = require('./model');
const Post = require('./model').Post;
const cookieKey = 'sid';
const redis = require("redis"),
    client = redis.createClient();

const isLoggedIn = (req, res, next) => {
    const sid = req.cookies[cookieKey]
    if (!sid) {
        return res.sendStatus(401)
    }
    client.get(sid, function (err, username) {
        if (err) throw err;
        if (username) {
            next()
        } else {
            res.sendStatus(401)
        }
    })
}
/* GET users listing. */
router.get('/:authors?', function(req, res) {
    const authors = req.params.authors ? req.params.authors.split(',') : null
    if(!authors) {
        let user;
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        });
        Post.findOne({author: user}, function (err, p) {
            res.status(200).send(p)
        })
    }else {
        Post.find({author: {$in: authors}}, function (err, posts) {
            if (!posts || posts.length === 0) {
                res.status(400).send('cannot find posts');
                return
            }
            const ret = [];
            posts.forEach(e => ret.push({id: e._id, author: e.author, text: e.text, img: e.img, date: e.date, comments: e.comments}))
            res.status(200).send({posts: ret})
        });
    }
});


router.post('/', isLoggedIn, function(req, res) {
    const author = req.body.author;
    const img = req.body.img;
    const date = req.body.date;
    const text = req.body.text;

    const newPost = new Post({
        author: author,
        img: img,
        date: date,
        text: text,
    });

    model.createPost(newPost, function(err, post){
        if(err) throw err;
        res.status(200).send({post: post,status:'success'})
    });
});

module.exports = router;
