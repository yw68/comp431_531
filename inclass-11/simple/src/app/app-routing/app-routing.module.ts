import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { DonateComponent } from '../donate/donate.component';
import { AboutComponent } from '../about/about.component';
import { ContactComponent } from '../contact/contact.component';
import { HomeComponent } from '../home/home.component';
import { ListComponent } from '../list/list.component';
import { ListingserviceService } from '../listingservice.service'


export const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'Full'},
  {path: 'donate', component: DonateComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'home', component: HomeComponent},
  {path: 'list', component: ListComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
