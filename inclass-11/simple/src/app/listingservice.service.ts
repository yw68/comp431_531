import { Injectable } from '@angular/core';

@Injectable()
export class ListingserviceService {

  constructor() { }

  getComponents() {
    return [
      {
        name: 'app',
        description: 'The main component'
      },
      {
        name: 'donate',
        description: 'The donate component'
      },
      {
        name: 'about',
        description: 'The about component'
      },
      {
        name: 'contact',
        description: 'The contact component'
      },
      {
        name: 'home',
        description: 'The home component'
      },
      {
        name: 'list',
        description: 'The list component'
      }
    ];
  }
}
