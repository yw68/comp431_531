import { TestBed, inject } from '@angular/core/testing';

import { ListingserviceService } from './listingservice.service';

describe('ListingserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListingserviceService]
    });
  });

  it('should be created', inject([ListingserviceService], (service: ListingserviceService) => {
    expect(service).toBeTruthy();
  }));
});
