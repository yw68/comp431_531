const express = require('express');
const router = express.Router();
const User = require('./model').User;
const cookieKey = 'sid';
const redis = require("redis"),
    client = redis.createClient({host: 'redis-14011.c11.us-east-1-2.ec2.cloud.redislabs.com', port: '14011'});

const isLoggedIn = (req, res, next) => {
    const sid = req.cookies[cookieKey];
    if (!sid) {
        return res.sendStatus(401)
    }
    client.get(sid, function (err, username) {
        if (err) throw err;
        if (username) {
            next()
        } else {
            res.sendStatus(401)
        }
    })
};

/* GET users listing. */
router.get('/:user?', isLoggedIn, function (req, res) {
    const user = req.params.user;
    if (!user) {
        let userLogin;
        client.get(req.cookies[cookieKey], function (err, u) {
            if (err) throw err;
            if (u) {
                userLogin = u;
            }
            User.findOne({accountName: userLogin}, function (err, u) {
                res.status(200).send({following: u.following})
            })
        });

    } else {
        User.findOne({accountName: user}, function (err, user) {
            res.status(200).send({following: user.following})
        });
    }
});
router.put('/', function (req, res, next) {
    let user = req.body.accountName;
    let following1 = [];
    User.findOne({accountName: req.body.follower}, function (err, u) {
        if(u == null) {
            res.status(200).send({result: 'Failure'});
        }
        else {
            User.findOne({accountName: user}, function (err, u) {
                following1 = u.following;
                following1.push(req.body.follower);
                User.findOneAndUpdate(
                    {accountName: user},
                    {$set: {following: following1}},
                    {upsert: false},
                    function (err, newUser) {
                    }
                );
                if (err) res.send(err);
                else {
                    res.status(200).send({result: 'Success'});
                }
            })
        }
    });
});
router.delete('/', function (req, res, next) {
    let user = req.body.accountName;
    let following = [];
    User.findOne({accountName: user}, function (err, u) {
        following = u.following.filter(e => e !== req.body.follower);
        User.findOneAndUpdate(
            {accountName: req.body.accountName},
            {$set: {following: following}},
            {upsert: false},
            function (err, newUser) {
                if (err) res.send(err);
                else {
                    res.status(200).send({following: newUser.following});
                }
            }
        )
    })
});

module.exports = router;