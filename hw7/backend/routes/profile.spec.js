const expect = require('chai');
const fetch = require('isomorphic-fetch');

const url = (path) => `http://127.0.0.1:3000${path}`;

describe('Validate Profile functionality', () => {

    it('should update headline', () => {
        let preHeadline;
        let newHeadline = "new headline";
        fetch(`http://localhost:3000/auth/login`,{
            method:'POST',
            headers:{
                'Content-Type':'application/json'
            },
            withCredentials: true,
            body: JSON.stringify({accountName: 'test', psw: 'yw99'})
        })
            .then(res => {
                return res.json();
            })
            .then(res => {
                expect(res.status).to.eql(500);
                return res;
            })
            .then(
                () => {
                    fetch(url("/profile/headlines/"), {
                        "method" : 'GET',
                        "headers": {
                            'Content-Type' : 'application/json',
                            'Access-Control-Allow-Origin':'localhost'
                        }
                    })
                        .then(res => {
                            preHeadline = res.body.headlines;
                        })

                        .then(() =>{
                            return fetch(url("/profile/headlines/"), {
                                "headers": {'Content-Type': 'application/json'},
                                "method": 'PUT',
                                "body": {accountName: 'test',headline: newHeadline}
                            })
                        })
                        .then((res)=>{
                            expect(res.body.headline).to.equal(newHeadline);
                        })
                        .then(() =>{
                            return fetch(url("/profile/headlines/" + 'test'), {
                                "method": 'GET',
                                "headers": {'Content-Type': 'application/json'}
                            })
                        })
                        .then(res => {
                            expect(res.body.headlines).to.equal(newHeadline);
                            //expect(res.body.headlines).to.not.equal(preHeadline);
                        })
                }
            )
    }, 200);

    // it('should update email or zip', (done) => {
    //     let preEmail;
    //     let latEmail = "new@test.com"
    //     fetch(url("/email"), {
    //         "method" : 'GET',
    //         "headers": {'Content-Type' : 'application/json' }
    //     })
    //         .then(res => {
    //             expect(res.status).to.eql(200);
    //             return res.json();
    //         })
    //         .then(res => {
    //             preEmail = res.email;
    //         })
    //
    //         .then(() =>{
    //             return fetch(url("/email"), {
    //                 "headers": {'Content-Type': 'application/json'},
    //                 "method": 'PUT',
    //                 "body": JSON.stringify({accountName: 'yw68',email: latEmail})
    //             });
    //         })
    //         .then(res=>{
    //             expect(res.status).to.equal(200);
    //         })
    //         .then((res)=>{
    //             expect(res.email).to.equal(latEmail)
    //         })
    //
    //         .then(_=>{
    //             return fetch(url("/email" + 'yw68'), {
    //                 "method": 'GET',
    //                 "headers": {'Content-Type': 'application/json'}
    //             })
    //         })
    //         .then(res => {
    //             expect(res.status).to.equal(200)
    //             return res.json()
    //         })
    //         .then(res => {
    //             expect(res.email).to.equal(latEmail)
    //             expect(res.email).to.not.equal(preEmail)
    //         })
    //         .then(done)
    //         .catch(done)
    // }, 200)
    //
    // it('content (headline, profile info, articles, etc) is only viewable by logged in users', (done) => {
    //     fetch(url("/email"), {
    //         "method" : 'GET',
    //         "headers": {'Content-Type' : 'application/json' },
    //         "cookies": {'sid': 'fake'}
    //     })
    //         .then(res => {
    //             expect(res.status).to.eql(401)
    //         })
    //         .then(done)
    //         .catch(done)
    // }, 200)
    //
    // it('must login again to view content', (done) => {
    //     fetch(url("/login"), {
    //         "method" : 'PUT',
    //         "headers": {'Content-Type' : 'application/json' },
    //         "accountName": 'yw69',
    //         "psw": 'yw69'
    //     })
    //         .then(res => {
    //             expect(res.status).to.eql(200)
    //         })
    //         .then(_=>{
    //             return fetch(url("/headlines/" + 'yw68'), {
    //                 "method": 'GET',
    //                 "headers": {'Content-Type': 'application/json'}
    //             })
    //         })
    //         .then(res => {
    //             expect(res.status).to.equal(200)
    //             return res.json()
    //         })
    //         .then(done)
    //         .catch(done)
    // }, 200)

})