const express = require('express');
const router = express.Router();
const User = require('./model').User;
const cookieKey = 'sid';
const redis = require("redis"),
    client = redis.createClient({host: 'redis-14011.c11.us-east-1-2.ec2.cloud.redislabs.com', port: '14011'});

const isLoggedIn = (req, res, next) => {
    const sid = req.cookies[cookieKey]
    if (!sid) {
        return res.sendStatus(401)
    }
    client.get(sid, function (err, username) {
        if (err) throw err;
        if (username) {
            next()
        } else {
            res.sendStatus(401)
        }
    })
}
/* GET users listing. */
router.get('/headlines/:users?', isLoggedIn, function (req, res, next) {
    const users = req.params.users ? req.params.users.split(',') : null
    if(!users) {
        let user
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        })
        User.findOne({accountName: user}, function (err, u) {
            res.status(200).send({headlines: u.headline})
        })
    }else {
        User.find({accountName: {$in: users}}, function (err, users) {
            if (!users || users.length === 0) {
                res.status(400).send('cannot find headlines')
                return
            }
            const ret = []
            users.forEach(e => ret.push({username: e.accountName, headline: e.headline}))
            res.status(200).send({headlines: ret})
        });
    }
});
router.put('/headline', isLoggedIn, function (req, res, next) { //update by name
    User.findOneAndUpdate(
        {accountName: req.body.accountName},
        {$set: {headline: req.body.headline}},
        {upsert: false},
        function (err, newUser) {
            if (err) res.send(err);
            else {
                res.status(200).send({headline: newUser.headline});
            }
        }
    )
});
router.get('/email/:user?', isLoggedIn, function (req, res, next) {
    const user = req.params.user;
    if(!user) {
        let user
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        })
        User.findOne({accountName: user}, function (err, u) {
            res.status(200).send({email: u.email})
        })
    }else {
        User.findOne({accountName: user}, function (err, user) {
            res.status(200).send({email: user.email})
        });
    }
});
router.put('/email', isLoggedIn, function (req, res, next) {
    User.findOneAndUpdate(
        {accountName: req.body.accountName},
        {$set: {email: req.body.email}},
        {upsert: false},
        function (err, newUser) {
            if (err) res.send(err);
            else {
                res.status(200).send({email: newUser.email});
            }
        }
    )
});
router.get('/phone/:user?', isLoggedIn, function (req, res, next) {
    const user = req.params.user
    if(!user) {
        let user
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        })
        User.findOne({accountName: user}, function (err, u) {
            res.status(200).send({phoneNumber: u.phoneNumber})
        })
    }else {
        User.findOne({accountName: user}, function (err, user) {
            res.status(200).send({phoneNumber: user.phoneNumber})
        });
    }
});
router.get('/dob/:user?', isLoggedIn, function (req, res, next) {
    const user = req.params.user
    if(!user) {
        let user;
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        });
        User.findOne({accountName: user}, function (err, u) {
            res.status(200).send({dob: u.dob})
        })
    }else {
        User.findOne({accountName: user}, function (err, user) {
            res.status(200).send({dob: user.dob})
        });
    }
});
router.get('/zip/:user?', isLoggedIn, function (req, res, next) {
    const user = req.params.user;
    if(!user) {
        let user
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
        })
        User.findOne({accountName: user}, function (err, u) {
            res.status(200).send({zip: u.zip})
        })
    }else {
        User.findOne({accountName: user}, function (err, user) {
            res.status(200).send({zip: user.zip})
        });
    }
});
router.put('/zip', isLoggedIn, function (req, res, next) {
    User.findOneAndUpdate(
        {accountName: req.body.accountName},
        {$set: {zip: req.body.zip}},
        {upsert: false},
        function (err, newUser) {
            if (err) res.send(err);
            else {
                res.status(200).send(newUser);
            }
        }
    )
});
router.get('/avatars/:users?', isLoggedIn, function (req, res, next) {
    const users = req.params.users ? req.params.users.split(',') : null
    if(!users) {
        let user
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u
            }
            User.findOne({accountName: user}, function (err, u) {
                res.status(200).send({avatars: u.avatar})
            })
        })
    }else {
        User.find({accountName: {$in: users}}, function (err, users) {
            if (!users || users.length === 0) {
                res.status(400).send('cannot find avatars')
                return
            }
            const ret = []
            users.forEach(e => ret.push({username: e.accountName, avatar: e.avatar}))
            res.status(200).send({avatars: ret})
        });
    }
});
router.put('/avatar', isLoggedIn, function (req, res, next) {
    User.findOneAndUpdate(
        {accountName: req.body.accountName},
        {$set: {avatar: req.body.avatar}},
        {upsert: false},
        function (err, newUser) {
            if (err) res.send(err);
            else {
                res.status(200).send(newUser);
            }
        }
    )
});
router.put('/edit', isLoggedIn, function (req, res, next) {
    User.findOneAndUpdate(
        {accountName: req.body.accountName},
        {$set: {
            displayName: req.body.displayName,
            phoneNumber: req.body.phoneNumber,
            email: req.body.email,
            zip: req.body.zip
        }},
        {upsert: false},
        function (err, newUser) {
            if (err) res.send(err);
            else {
                res.status(200).send(newUser)
            }
        }
    )
});
router.get('/user', isLoggedIn, function (req, res, next) {
    let user
    client.get(req.cookies[cookieKey], function(err, u) {
        if (err) throw err;
        if (u) {
            user = u
            User.findOne({accountName: user}, function (err, u) {
                res.status(200).send({
                    displayName: u.displayName,
                    phoneNumber: u.phoneNumber,
                    email: u.email,
                    zip: u.zip,
                    dob: u.dob,
                    avatar: u.avatar,
                    headline: u.headline
                })
            })
        }
    })
});


module.exports = router;