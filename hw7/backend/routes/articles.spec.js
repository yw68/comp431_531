const expect = require('chai').expect
const fetch = require('isomorphic-fetch')

const url = path => `http://localhost:3000${path}`

describe('Validate Article functionality', () => {

    // it('should give me three or more articles', (done) => {
    //     fetch(url("/articles/test,xingliu"))
    //         .then(res=>{
    //             expect(res.status).to.eql(200)
    //             return res.json()
    //         })
    //         .then(body=>{
    //             expect(body.articles.length).to.be.at.least(3)
    //         })
    //         .then(done)
    //         .catch(err=>{
    //             throw new Error(err)
    //         })
    // }, 200)
    //
    // it('should return an article with a specified id', (done) => {
    //     fetch(url("/articles/test"))
    //         .then(res =>{
    //             expect(res.status).to.eql(200)
    //             return res.json()
    //         })
    //         .then(body=>{
    //             expect(body.articles.length).to.eql(1)
    //         })
    //         .then(done)
    //         .catch(err=>{
    //             throw new Error(err)
    //         })
    // }, 200)
    //
    // it('should return nothing for an invalid id', (done) => {
    //     // call GET /articles/id where id is not a valid article id, perhaps 0
    //     fetch(url("/articles/-1"))
    //         .then(res => {
    //             expect(res.status).to.eql(400)
    //         })
    //         .then(done)
    //         .catch(done)
    //
    // }, 200)

    it('should add an article and test the new id and text', () => {
        const postArticle = {author:'yw69', text: 'new posted article'}


        fetch(`http://localhost:3000/auth/login`,{
            method:'POST',
            headers:{
                'Content-Type':'application/json',
                'Access-Control-Allow-Origin':'localhost'
            },
            withCredentials: true,
            body: JSON.stringify({accountName: 'test', psw: 'yw99'})
        })
            .then(res => {console.log('123123123123123'); return res.json();})
            .then(res => {
                console.log(res);
                expect(res.result).to.equal('Success')
            })
            .then(() => {
                fetch(url('/articles/'),{
                    method:'POST',
                    headers:{
                        'Content-Type':'application/json'
                    },
                    body: JSON.stringify(postArticle)
                })
                    .then(res => {
                        console.log(res.json());
                        return res.json();})
                    .then(res => {
                        expect(res.status).to.eql('success')
                    })
            })

        // fetch(url("/articles"))
        //     .then(res => {
        //         expect(res.status).to.eql(200)
        //         return res.json()
        //     })
        //     .then(body => {
        //         expect(body.post.length).to.be.at.least(postArticle.length + 1)
        //     })
        //     .then(done)
        //     .catch(done)
    }, 200)
});