import { browser, by, element } from 'protractor';

export class MainPage {
  navigateTo() {
    browser.get('/#/login');
    element(by.id('account')).sendKeys('test');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('loginBTN')).click();
    browser.sleep(500);
    browser.sleep(200);
  }
  createArticle() {
    element(by.id('inputArticle')).sendKeys('This is a new article');
    element(by.id('postBTN')).click();
    browser.sleep(200);
    return element.all(by.id('article')).get(0).getText();
  }
  updateHeadline() {
    element(by.id('headlineInput')).sendKeys('This is a new headline');
    element(by.id('headlineBtn')).click();
    browser.sleep(200);
    return element(by.id('headline')).getText();
  }
  countFollower() {
    const count = element.all(by.id('follower')).count();
    browser.sleep(200);
    return count;
  }
  addFollower() {
    element(by.id('followerInput')).sendKeys('xingliu');
    element(by.id('followerBtn')).click();
    browser.sleep(200);
    return element.all(by.id('follower')).count();
  }
  deleteFollower() {
    element(by.id('xingliudeleteBtn')).click();
    browser.sleep(200);
    return element.all(by.id('follower')).count();
  }
  searchArticle() { // how to get the other like display
    element(by.id('searchInput')).sendKeys('!!!!!!!!!!!!!!!!');
    element(by.id('searchBtn')).click();
    browser.sleep(200);
    return element.all(by.id('searchArticle')).count();
  }
}
