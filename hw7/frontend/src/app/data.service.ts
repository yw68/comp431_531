import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { RequestOptions } from '@angular/http';
import { RequestMethod } from '@angular/http';
import { Article } from "./article";
import 'rxjs/add/operator/toPromise';


const url = (path) => `https://slim-web.herokuapp.com${path}`;
@Injectable()
export class DataService {
  private handleError(error: any): Promise<any> {
    console.error('An HTTP error occurred', error);
    return Promise.reject(error.message || error);
  }
  constructor(private http: Http) { }
  // uploadPictrue() {
  //   this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any) => {
  //     this.cloudinaryImage = JSON.parse(response);
  //     return {item, response, status, headers};
  //   };
  // }
  addUser(accountName: string, phoneNumber: string, zip: string, email: string, psw: string, dateOfBirth: Date, displayName?: string) {
    //  const u = new User(id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, display, displayName);
    const body = JSON.stringify({
      accountName: accountName,
      phoneNumber: phoneNumber,
      displayName: displayName,
      zip: zip,
      dob: dateOfBirth,
      email: email,
      psw: psw
    }); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Post, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/auth/register'), options)
      .toPromise()
      .then(res => console.log(res.json())) //  res:{result: 'success', username: accountName}
      .catch(this.handleError);
  }
  loginUser(accountName: string, psw: string): Promise<boolean> {
    const body = JSON.stringify({accountName: accountName, psw: psw}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Post, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/auth/login'), options)
      .toPromise()
      .then(res => {
        console.log(res.json());
        return res;
      })
      .then(res => res.json().result === 'Success')
      .catch(this.handleError);
  }
  getArticles(authors: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/articles/' + authors), options)
      .toPromise()
      .then(res => res.json())
      .then(res => res.posts as Article[])  //  will they match??????
      .catch(this.handleError);
  }
  getFollowers() {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/following'), options)
      .toPromise()
      .then(response => response.json().following as string[])
      .catch(this.handleError);
  }
  deleteFollower(user: string, follower: string) {
    const body = JSON.stringify({accountName: user, follower: follower}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Delete, headers: headers, withCredentials: true, body: body }); // Create a request option

    return this.http.request(url('/following'), options)
      .toPromise()
      .then(res => res.json().following as string[])
      .then(res => console.log(res))  //  followers after deleting
      .catch(this.handleError);
  }
  addFollower(user: string, follower: string): Promise<string> {
    const body = JSON.stringify({accountName: user, follower: follower}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/following'), options)
      .toPromise()
      .then(res => res.json().result)
      //  .then(res => console.log(res))  //  followers after adding
      .catch(this.handleError);
  }
  addPost(author: string, content: string, date: string, picture?: string): Promise<number> {
    const body = JSON.stringify({author: author, text: content, date: date, img: picture}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Post, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/articles'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);
  }
  addComment(author: string, content: string, date: Date, id: string): Promise<number> {
    const body = JSON.stringify({author: author, text: content, date: date, id: id}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Post, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/articles/comment'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);
  }
  editUser(accountName: string, phoneNumber: string, zip: string, email: string, psw: string, displayName?: string) {
    let body = JSON.stringify({
      accountName: accountName,
      displayName: displayName,
      phoneNumber: phoneNumber,
      email: email,
      zip: zip}); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    this.http.request(url('/profile/edit'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);

    body = JSON.stringify({accountName: accountName, psw: psw}); // Stringify payload
    headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    this.http.request(url('/auth/password'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);
  }
  getUser() {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/profile/user'), options)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }
  setStatus(accountName: string, headline: string) {
    const body = JSON.stringify({accountName: accountName, headline: headline}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/profile/headline'), options)
      .toPromise()
      .then(res => console.log(res.json().headline))
      .catch(this.handleError);
  }
  getHeadline(accountNames: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/profile/headlines/' + accountNames), options)
      .toPromise()
      .then(response => response.json().headlines as string[])
      .catch(this.handleError);
  }
  getAvatar(accountNames: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/profile/avatars/' + accountNames), options)
      .toPromise()
      .then(response => response.json().avatars as string[])
      .catch(this.handleError);
  }
  getDob(accountNames: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/profile/dob/' + accountNames), options)
      .toPromise()
      .then(response => response.json().avatars as string[])
      .catch(this.handleError);
  }
  getZip(accountNames: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/profile/zip/' + accountNames), options)
      .toPromise()
      .then(response => response.json().avatars as string[])
      .catch(this.handleError);
  }
  getPhone(accountNames: string) {
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.get(url('/profile/phone/' + accountNames), options)
      .toPromise()
      .then(response => response.json().avatars as string[])
      .catch(this.handleError);
  }
  updateAvatar(accountName: string, avatar: string) {
    const body = JSON.stringify({accountName: accountName, avatar: avatar}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/profile/avatar'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);
  }
  updateArticle(articleId: string, text: string) {
    const body = JSON.stringify({text: text, id: articleId}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/articles/'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);
  }
  updateComment(articleId: string, text: string, author: string, date: string) {
    const body = JSON.stringify({text: text, id: articleId, author: author, date: date}); // Stringify payload
    const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    const options = new RequestOptions({ method: RequestMethod.Put, headers: headers, withCredentials: true, body: body }); // Create a request option
    return this.http.request(url('/articles/comment'), options)
      .toPromise()
      .then(res => console.log(res.json()))
      .catch(this.handleError);
  }
}
