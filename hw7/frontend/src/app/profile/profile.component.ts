import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { DataService } from "../data.service";
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions({ cloudName: 'yw68', uploadPreset: 'agfevxpi' })
  );
  userLogin: string;
  model: User;
  current: User;
  display: boolean;
  unlinkD: boolean;
  uploadAvatar() {
    this.uploader.uploadAll();
  }
  constructor(private dataService: DataService) {
  }
  link(name: string, psw: string) {
    return this.dataService.loginUser(name, psw)
      .then(res => {
        if (res) {
          this.dataService.getAvatar(name)
            .then(r => this.model.img = r[0])
            .then(() => this.dataService.editUser(this.userLogin, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.displayName))
            .then(() => this.current.img = this.model.img);
          this.dataService.getHeadline(name)
            .then(r => this.model.status = r[0])
            .then(() => this.dataService.editUser(this.userLogin, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.displayName))
            .then(() => this.current.status = this.model.status);
          this.dataService.getZip(name)
            .then(r => this.model.zip = r[0])
            .then(() => this.dataService.editUser(this.userLogin, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.displayName))
            .then(() => this.current.zip = this.model.zip);
          this.dataService.getDob(name)
            .then(r => this.model.dateOfBirth = r[0])
            .then(() => this.dataService.editUser(this.userLogin, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.displayName))
            .then(() => this.current.dateOfBirth = this.model.dateOfBirth);
          this.dataService.getPhone(name)
            .then(r => this.model.phoneNumber = r[0])
            .then(() => this.dataService.editUser(this.userLogin, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.displayName))
            .then(() => this.current.phoneNumber = this.model.phoneNumber);
        } else this.display = true;
        return res;
      });
  }
  unlink() {
    this.unlinkD = true;
  }
  onSubmit(): void {
    this.dataService.editUser(this.userLogin, this.current.phoneNumber, this.current.zip, this.current.email, this.current.psw, this.current.displayName);
    this.model.displayName = this.current.displayName;
    this.model.phoneNumber = this.current.phoneNumber;
    this.model.zip = this.current.zip;
    this.model.email = this.current.email;
    this.model.psw = this.current.psw;
    this.model.repsw = this.current.repsw;
    this.current = new User(0, '', '', '', '', '', '', null, '', '', [], [], '');
  }
  ngOnInit() {
    this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any): any => {
      const res: any = JSON.parse(response);
      this.current.img = res.url;
      this.model.img = res.url;
      console.log(this.current.img);
      this.dataService.updateAvatar(localStorage.getItem('userLogin'), this.model.img);
      return { item, response, status, headers };
    };
    this.display = false;
    this.unlinkD = false;
    this.model = new User(0, '', '', '', '', '', '', null, '', '', [], [], '');
    this.current = new User(0, '', '', '', '', '', '', null, '', '', [], [], '');
    this.userLogin = localStorage.getItem('userLogin');
    this.dataService.getUser().then(user => {
      this.model.displayName = user.displayName;
      this.model.email = user.email;
      this.model.dateOfBirth = user.dob.slice(0, 10);
      this.model.zip = user.zip;
      this.model.phoneNumber = user.phoneNumber;
      console.log(this.model);
    });
    this.dataService.getAvatar('').then(res => this.model.img = res);
  }

}
