import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup } from "@angular/forms";
import { DataService } from '../data.service';
import { Router } from "@angular/router";
import {error} from "util";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  display: boolean;
  loginForm = new FormGroup ({
    name: new FormControl('', [
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  });

  constructor(private dataService: DataService, private router: Router) {
  }

  ngOnInit() {
    this.display = false;
  }
  login(name: string, psw: string) {
    return this.dataService.loginUser(name, psw)
      .then(res => {
        if (res) {
          localStorage.setItem('userLogin', name);
          this.router.navigate(['./main']);
        } else this.display = true;
        return res;
      });
  }
  redirect() {
    this.router.navigate(['./google']);
  }
  // login(name: string, psw: string) {
  //   const res = this.dataService.loginUser(name, psw);
  //   console.log(res);
  // }
}
