import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from 'angular4-social-login';
import { SocialUser } from 'angular4-social-login';
import { DataService } from '../data.service';
import { GoogleLoginProvider } from 'angular4-social-login';

@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.css']
})
export class GoogleComponent implements OnInit {

  user: SocialUser;

  constructor(private authService: AuthService, private router: Router, private dataService: DataService) { }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      if (this.user) {
        console.log(123123);
        this.succeed(this.user.photoUrl, this.user.email, this.user.name);
      }
    });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  succeed(url: string, email: string, name: string): void {
    const date = new Date('1990-01-01');
    this.dataService.addUser(email, '000-000-0000', '00000', email, email, date, name)
      .then(_ => this.dataService.loginUser(email, email))
      .then(_ => this.dataService.updateAvatar(email, url))
      .then(() => {
        localStorage.setItem('userLogin', email);
        this.router.navigate(['./main']);
      });
  }

  failure(): void {
  }

  signOut(): void {
    this.authService.signOut();
  }

}
