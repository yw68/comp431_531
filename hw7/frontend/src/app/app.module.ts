import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ProfileComponent } from './profile/profile.component';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { MainComponent } from './main/main.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { EqualValidator } from './validator/equal-validator.directive';
import { DataService } from "./data.service";
import { AgeValidator } from './validator/age-validator.directive';
import { HttpClientModule } from "@angular/common/http";
import { Ng2CloudinaryModule } from '/Users/wang/hw5/node_modules/ng2-cloudinary';
import { FileUploadModule } from 'ng2-file-upload';
import { SocialLoginModule } from 'angular4-social-login';
import { AuthServiceConfig, GoogleLoginProvider } from 'angular4-social-login';
import { GoogleComponent } from './google/google.component';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('1034019523620-6csrde1249rvga4e4rp4dbnvt32efju0.apps.googleusercontent.com')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    MainComponent,
    ProfileComponent,
    RegistrationComponent,
    LoginComponent,
    EqualValidator,
    AgeValidator,
    GoogleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    Ng2CloudinaryModule,
    FileUploadModule,
    SocialLoginModule
  ],
  providers: [
    DataService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
