export class Comment {
  text: string;
  date: Date;
  author: string;

  constructor(text: string,
              date: Date,
              author: string) {
    this.text = text;
    this.date = date;
    this.author = author;
  }
}
