import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[validateAge][formControlName],[validateAge][formControl],[validateAge][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => AgeValidator), multi: true }
  ]
})
export class AgeValidator implements Validator {
  constructor(@Attribute('validateAge') public validateEqual: string) {
  }

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    if(c.value == null) {
      return {
        validateEqual: false
      }
    }
    let v = parseInt(c.value.slice(0,4), 10);
    if (new Date().getFullYear() - v < 18) {
      return {
        validateEqual: false
      }
    }
    return null;
  }
}
