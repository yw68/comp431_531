export class User {

  constructor(
    public account: string,
    public email: string,
    public phone: string,
    public birthday: string,
    public zip: string,
    public password: string,
    public confirmPassword: string,
    public display?: string
  ) {  }

}
