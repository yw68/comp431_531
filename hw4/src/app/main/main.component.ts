import { Component, OnInit } from '@angular/core';

import { ArticlesService } from '../articles.service';
import { Article } from "../article";
import { Follower } from "../follower";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  user={
    name: "user",
    status: "I'm happy"
  };
  articles: Article[];
  followers: Follower[];

  constructor(private articlesService: ArticlesService) { }

  getArticles(): void {
    this.articlesService
      .getArticles()
      .then(articles => this.articles = articles)
      .then(()=> console.log(this.articles));
  }

  filterArticles(inputKey: string): void {
    if(inputKey != null) this.articles.forEach(function(element) {
      if(element.author.indexOf(inputKey) == -1 && element.text.indexOf(inputKey) == -1) element.display = false;
    });
    if(inputKey == '') this.articles.forEach(x => x.display = true)
  }

  deleteFollowers(name: String): void {
    this.followers.find(e => e.name === name).display = false;
  }

  getFollowers(): void {
    this.articlesService
      .getFollowers()
      .then(followers => this.followers = followers)
      .then(()=> console.log(this.followers));
  }

  addFriend(inputAdd: string): void {
    this.followers.push(new Follower(inputAdd, 'Hellow World', 'https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAAgdAAAAJGM0ZDE0ZGIzLTFlMWEtNDk1Ni1iMDY2LWE1Y2RlZDczYmZkZA.png', true))
  }

  post(inputText: string): void {
    this.articles.unshift(new Article(12, inputText, new Date().toString(), '', [], this.user.name, true));
  }

  updateStatus(inputStatus: string): void {
    this.user.status=inputStatus;
  }
  ngOnInit() {
    this.getArticles();
    this.getFollowers();
  }
}
