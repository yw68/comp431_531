export class Article {
  id: number;
  text: string;
  date: string;
  img: string;
  comments: string[];
  author: string;
  display: boolean;

  constructor(id: number,
              text: string,
              date: string,
              img: string,
              comments: string[],
              author: string,
              display: boolean) {
    this.id = id;
    this.text = text;
    this.date = date;
    this.img = img;
    this.comments = comments;
    this.author = author;
    this.display= display;
  }
}
