import { Component, OnInit } from '@angular/core';
import { User }    from '../user';
import {current} from "codelyzer/util/syntaxKind";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  model: User;
  current: User;
  constructor() {

  }

  onSubmit(): void {
    this.model.display = this.current.display;
    this.model.email = this.current.email;
    this.model.phone = this.current.phone;
    this.model.zip = this.current.zip;
  }
  ngOnInit() {
    this.model = new User('user1', 'hero@gmail.com', '123-123-1234', '05/05/2005', '12345', '123456', '123456', 'hero');
    this.current = new User('', '', '', '', '', '', '','')
  }

}
