import { Component, OnInit } from '@angular/core';
import { User }    from '../user';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  users: User[];
  model = new User('', '', '', '', '', '', '', '');
  submitted = false;

  ngOnInit() {
    this.submitted = true;
    //this.users.push(this.model);
  }
  newUser() {
    this.model = new User('', '', '', '', '', '', '', '');
  }

}
