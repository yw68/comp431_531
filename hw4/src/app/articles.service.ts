import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Article } from "./article";
import { Follower } from "./follower";


@Injectable()
export class ArticlesService {

  private articlesUrl = 'api/data';

  private handleError(error: any): Promise<any> {
    console.error('An HTTP error occurred', error);
    return Promise.reject(error.message || error);
  }

  constructor(private http: Http) { }

  getArticles(): Promise<Article[]> {
    return this.http.get(this.articlesUrl)
      .toPromise()
      .then(response => response.json().articles as Article[])
      .catch(this.handleError);
  }
  getFollowers(): Promise<Follower[]> {
    return this.http.get(this.articlesUrl)
      .toPromise()
      .then(response => response.json().followers as Follower[])
      .catch(this.handleError);
  }
}
