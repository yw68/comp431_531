const express = require('express');
const bodyParser = require('body-parser');


let flag = 4;

let articles = [
 {
  id: 1,
  author: "Smith",
  text: "I am Smith."
 },
 {
  id: 2,
  author: "John",
  text: "I am John."
 },
 {
  id: 3,
  author: "Lily",
  text: "Hello."
 }
];

const hello = (req, res) => res.send({ hello: 'world' });

const getArticles = (req, res) => res.send({articles: articles});

const addArticle = (req, res) => {
 let article = {
  id: flag++,
  author: req.body.author,
  text: req.body.text
 };
 articles.push(article);

 res.send(article);
}


const app = express();
app.use(bodyParser.json());
app.get('/articles', getArticles);
app.post('/article', addArticle);
app.get('/', hello);

// Get the port from the environment, i.e., Heroku sets it
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
 const addr = server.address()
 console.log(`Server listening at localhost:${addr.port}`)
});