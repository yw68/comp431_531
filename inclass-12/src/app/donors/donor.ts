export class Donor {
  id: number;
  name: string;
  image: string;
  featured: boolean;
  description: string;

  constructor(id: number,
              name: string,
              image: string,
              featured: boolean,
              description: string) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.featured = featured;
    this.description = description;
  }
}

export const donors: Donor[] = [
  new Donor(1, 'John', '/John.img', true, 'rich'),
  new Donor(2, 'Smith', '/Smith.img', true, 'common'),
  new Donor(3, 'Jesse', '/Jesse.img', true, 'poor')
]

