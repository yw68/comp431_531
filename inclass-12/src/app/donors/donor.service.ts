import {Injectable} from '@angular/core';
import {Donor} from './donor';

@Injectable()
export class DonorService {

  donors: Donor[];

  constructor() {
  }

  getDonors() {
    return this.donors;
  }

  getFeaturedDonor() {
    return this.donors.find(d => d.featured);
  }

  getDonor(id) {
    return this.donors.find(d => d.id === id);
  }

}
