import { Component, OnInit } from '@angular/core';
import { ListingserviceService} from '../listingservice.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ListingserviceService]
})
export class ListComponent implements OnInit {

  public components;
  constructor(listingserviceService: ListingserviceService) {
    this.components = listingserviceService.getComponents();
  }

  ngOnInit() {
  }

}
