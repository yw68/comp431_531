import { Component, OnInit } from '@angular/core';
import {DonorService} from '../donors/donor.service';

@Component({
  selector: 'app-donate',
  templateUrl: './donate.component.html',
  styleUrls: ['./donate.component.css'],
  providers: [DonorService]
})
export class DonateComponent implements OnInit {

  public component;
  constructor(donorService: DonorService) {
    this.component = donorService.getFeaturedDonor();
  }

  ngOnInit() {
  }

}
