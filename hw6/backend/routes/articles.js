const express = require('express');
const router = express.Router();
const model = require('./model');
const Post = require('./model').Post;
const Comment = require('./model').Comment;
const cookieKey = 'sid';
const redis = require("redis"),
    client = redis.createClient({host: 'redis-14011.c11.us-east-1-2.ec2.cloud.redislabs.com', port: '14011'});

const isLoggedIn = (req, res, next) => {
    const sid = req.cookies[cookieKey]
    if (!sid) {
        return res.sendStatus(401)
    }
    client.get(sid, function (err, username) {
        if (err) throw err;
        if (username) {
            next()
        } else {
            res.sendStatus(401)
        }
    })
}
/* GET users listing. */
router.get('/:authors?', function(req, res) {
    const authors = req.params.authors ? req.params.authors.split(',') : null
    if(!authors) {
        let user;
        client.get(req.cookies[cookieKey], function(err, u) {
            if (err) throw err;
            if (u) {
                user = u;
            }
            Post.find({author: user}, function (err, p) {
                res.status(200).send(p)
            })
        });
    }else {
        Post.find({author: {$in: authors}}, function (err, posts) {
            if (!posts || posts.length === 0) {
                res.status(400).send('cannot find posts');
                return
            }
            const ret = [];
            posts.forEach(e => ret.push({id: e._id, author: e.author, text: e.text, img: e.img, date: e.date, comments: e.comments}))
            res.status(200).send({posts: ret})
        });
    }
});
router.post('/', isLoggedIn, function(req, res) {
    const author = req.body.author;
    const img = req.body.img;
    const date = req.body.date;
    const text = req.body.text;

    const newPost = new Post({
        author: author,
        img: img,
        date: date,
        text: text,
    });

    model.createPost(newPost, function(err, post){
        if(err) throw err;
        res.status(200).send({post: post,status:'success'})
    });
});
router.put('/', isLoggedIn, function(req, res) {
    const text = req.body.text;
    const postID = req.body.id;
    Post.findOneAndUpdate(
        {_id: postID},
        {$set: {text: text}},
        {upsert: false},
        function (err, newPost) {
            if (err) res.send(err);
            else {
                res.status(200).send({result: 'Success'});
            }
        }
    )
});
router.post('/comment', isLoggedIn, function (req, res) {
    const postID = req.body.id;
    const author = req.body.author;
    const date = req.body.date;
    const text = req.body.text;
    const newComment = new Comment({
        author: author,
        date: date,
        text: text
    });
    Post.findOne({_id: postID}, function (err, p) {
        let newComments = p.comments;
        newComments.push(newComment);
        Post.findOneAndUpdate(
            {_id: postID},
            {$set: {comments: newComments}},
            {upsert: false},
            function (err, newPost) {
            }
            );
        if (err) res.send(err);
        else {
            res.status(200).send({result: 'Success'});
        }
    })
});
router.put('/comment', isLoggedIn, function (req, res) {
    const postID = req.body.id;
    const text = req.body.text;
    const author = req.body.author;
    const date = req.body.date;
    Post.findOne({_id: postID}, function (err, p) {
        let newComments = p.comments;
        newComments.forEach(e => {
            if (e.author === author && Date.parse(date).toString().slice(0, 10) === Date.parse(e.date.toString()).toString().slice(0, 10)) {
                e.text = text;
            }
        });
        Post.findOneAndUpdate(
            {_id: postID},
            {$set: {comments: newComments}},
            {upsert: false},
            function (err, newPost) {
            }
        );
        if (err) res.send(err);
        else {
            res.status(200).send({result: 'Success'});
        }
    })
});

module.exports = router;
