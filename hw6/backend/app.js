var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var expressSession = require('express-session');
var cloudinary = require('cloudinary');


var index = require('./routes/index');
var users = require('./routes/users');
var articles = require('./routes/articles');
var auth = require('./routes/auth');
var profile = require('./routes/profile');
var following = require('./routes/following');

var app = express();

var uri = "mongodb://slimwangyue:yuege5991@comp531-shard-00-00-swmsd.mongodb.net:27017,comp531-shard-00-01-swmsd.mongodb.net:27017,comp531-shard-00-02-swmsd.mongodb.net:27017/test?ssl=true&replicaSet=comp531-shard-0&authSource=admin";
var mongoose = require('mongoose');

/* cloudinary config */
cloudinary.config({
    cloud_name: 'yw68',
    api_key: '137474236874393',
    api_secret: 'Pw2l92MVG4FTbqGLojEGxCBOhU8'
});

/* mongoose config */
mongoose.connect(uri, { useMongoClient: true });
mongoose.Promise = global.Promise;


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressValidator());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressSession({secret: 'max', saveUninitialized: false, resave: false}));

// app.use(function (req, res, next) {
//     req.header('Access-Control-Allow-Origin', 'http://localhost:4200');
//     req.header('Access-Control-Allow-Credentials', 'true');
//     req.header('Access-Control-Allow-Methods', 'GET', 'PUT', 'DELETE', 'POST','OPTIONS');
//     req.header('Access-Control-Allow-Headers', 'Content-Type', 'Authorization', 'X-Requested-With', 'X-Session-Id');
//     next();
// });
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', req.headers.origin);

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use(function (req, res, next) {
    if(req.method === 'OPTIONS') {
        res.sendStatus(200);}
    else next();
});
app.use('/', index);
app.use('/users', users);
app.use('/articles', articles);
app.use('/auth', auth);
app.use('/profile', profile);
app.use('/following', following);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
// set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

// render the error page
    res.status(err.status || 500);
    res.render('error');
});


module.exports = app;
