import { browser, by, element } from 'protractor';

export class AuthPage {
  navigateToLogin() {
    return browser.get('/#/login');
  }

  navigateToRegistration() {
    return browser.get('/#/registration');
  }

  login() {
    element(by.id('account')).sendKeys('test');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('loginBTN')).click();
    browser.sleep(500);
    return browser.getCurrentUrl();
  }
  registration() {
    element(by.id('account')).sendKeys('yw99');
    element(by.id('display')).sendKeys('superhero');
    element(by.id('email')).sendKeys('yw99@test.com');
    element(by.id('phone')).sendKeys('123-456-7890');
    element(by.id('birthday')).sendKeys('1990-10-10');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('repassword')).sendKeys('yw99');
    element(by.id('registrationBTN')).click();
    browser.sleep(500);
    return browser.getCurrentUrl();
  }
}
