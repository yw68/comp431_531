import { browser, by, element } from 'protractor';

export class ProfilePage {
  navigateTo() {
    browser.get('/#/login');
    element(by.id('account')).sendKeys('test');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('loginBTN')).click();
    browser.sleep(200);
    browser.get('/#/profile');
    browser.sleep(200);
  }
  updateEmail() { // how to get the other like display
    element(by.id('display')).sendKeys('superhero');
    element(by.id('email')).sendKeys('test@test.com');
    element(by.id('phone')).sendKeys('123-456-7890');
    element(by.id('zip')).sendKeys('77272');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('repassword')).sendKeys('yw99');
    element(by.id('submitBtn')).click();
    browser.sleep(200);
    return element(by.id('displayEmail')).getText();
  }
  updateZip() { // how to get the other like display
    element(by.id('display')).sendKeys('superhero');
    element(by.id('email')).sendKeys('yw99@test.com');
    element(by.id('phone')).sendKeys('123-456-7890');
    element(by.id('zip')).sendKeys('70000');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('repassword')).sendKeys('yw99');
    element(by.id('submitBtn')).click();
    browser.sleep(200);
    return element(by.id('displayZip')).getText();
  }
  updatePassword() {
    element(by.id('display')).sendKeys('superhero');
    element(by.id('email')).sendKeys('yw99@test.com');
    element(by.id('phone')).sendKeys('123-456-7890');
    element(by.id('zip')).sendKeys('70000');
    element(by.id('password')).sendKeys('yw99');
    element(by.id('repassword')).sendKeys('yw99');
    element(by.id('submitBtn')).click();
    browser.sleep(200);
    return 'Success';
  } //  how?
}
