import { MainPage } from './main.po';

describe('Main Page', () => {
  let page: MainPage;

  beforeEach(() => {
    page = new MainPage();
  });

  it('should create a new article', () => {
    page.navigateTo();
    expect(page.createArticle()).toEqual('This is a new article');
  });
  it('should update headline', () => {
    page.navigateTo();
    expect(page.updateHeadline()).toEqual('Status: This is a new headline');
  });
  it('count the follower', () => {
    page.navigateTo();
    expect(page.countFollower()).toEqual(0);
  });
  it('add follower', () => {
    page.navigateTo();
    expect(page.addFollower()).toEqual(1);
  });
  it('delete follower', () => {
    page.navigateTo();
    expect(page.deleteFollower()).toEqual(0);
  });
  it('search articles', () => {
    page.navigateTo();
    expect(page.searchArticle()).toEqual(0);  //  to be changed
  });
});
