webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__landing_landing_component__ = __webpack_require__("../../../../../src/app/landing/landing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profile_profile_component__ = __webpack_require__("../../../../../src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__registration_registration_component__ = __webpack_require__("../../../../../src/app/registration/registration.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    { path: '', redirectTo: '/landing', pathMatch: 'full' },
    { path: 'landing', component: __WEBPACK_IMPORTED_MODULE_3__landing_landing_component__["a" /* LandingComponent */] },
    { path: 'main', component: __WEBPACK_IMPORTED_MODULE_4__main_main_component__["a" /* MainComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_5__profile_profile_component__["a" /* ProfileComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_6__login_login_component__["a" /* LoginComponent */] },
    { path: 'registration', component: __WEBPACK_IMPORTED_MODULE_7__registration_registration_component__["a" /* RegistrationComponent */] }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(routes, { useHash: true })
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */]],
        declarations: []
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>{{title}}</h1>\n\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'hw5';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile_component__ = __webpack_require__("../../../../../src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__landing_landing_component__ = __webpack_require__("../../../../../src/app/landing/landing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routing_app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__registration_registration_component__ = __webpack_require__("../../../../../src/app/registration/registration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__validator_equal_validator_directive__ = __webpack_require__("../../../../../src/app/validator/equal-validator.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__validator_age_validator_directive__ = __webpack_require__("../../../../../src/app/validator/age-validator.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__Users_wang_hw5_node_modules_ng2_cloudinary__ = __webpack_require__("../../../../ng2-cloudinary/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_ng2_file_upload__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__landing_landing_component__["a" /* LandingComponent */],
            __WEBPACK_IMPORTED_MODULE_7__main_main_component__["a" /* MainComponent */],
            __WEBPACK_IMPORTED_MODULE_4__profile_profile_component__["a" /* ProfileComponent */],
            __WEBPACK_IMPORTED_MODULE_9__registration_registration_component__["a" /* RegistrationComponent */],
            __WEBPACK_IMPORTED_MODULE_10__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_11__validator_equal_validator_directive__["a" /* EqualValidator */],
            __WEBPACK_IMPORTED_MODULE_13__validator_age_validator_directive__["a" /* AgeValidator */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_14__angular_common_http__["a" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_8__app_routing_app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_15__Users_wang_hw5_node_modules_ng2_cloudinary__["c" /* Ng2CloudinaryModule */],
            __WEBPACK_IMPORTED_MODULE_16_ng2_file_upload__["FileUploadModule"]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_12__data_service__["a" /* DataService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/article.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Article; });
var Article = (function () {
    function Article(text, date, img, comments, author) {
        this.text = text;
        this.date = date;
        this.img = img;
        this.comments = comments;
        this.author = author;
        this.display = true;
    }
    return Article;
}());

//# sourceMappingURL=article.js.map

/***/ }),

/***/ "../../../../../src/app/comment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Comment; });
var Comment = (function () {
    function Comment(text, date, author) {
        this.text = text;
        this.date = date;
        this.author = author;
    }
    return Comment;
}());

//# sourceMappingURL=comment.js.map

/***/ }),

/***/ "../../../../../src/app/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var url = function (path) { return "https://slim-web.herokuapp.com" + path; };
var DataService = (function () {
    function DataService(http) {
        this.http = http;
    }
    DataService.prototype.handleError = function (error) {
        console.error('An HTTP error occurred', error);
        return Promise.reject(error.message || error);
    };
    // uploadPictrue() {
    //   this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any) => {
    //     this.cloudinaryImage = JSON.parse(response);
    //     return {item, response, status, headers};
    //   };
    // }
    DataService.prototype.addUser = function (accountName, phoneNumber, zip, email, psw, dateOfBirth, displayName) {
        //  const u = new User(id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, display, displayName);
        var body = JSON.stringify({
            accountName: accountName,
            phoneNumber: phoneNumber,
            displayName: displayName,
            zip: zip,
            dob: dateOfBirth,
            email: email,
            psw: psw
        }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Post, headers: headers, withCredentials: true, body: body }); // Create a request option
        this.http.request(url('/auth/register'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); }) //  res:{result: 'success', username: accountName}
            .catch(this.handleError);
    };
    DataService.prototype.loginUser = function (accountName, psw) {
        var body = JSON.stringify({ accountName: accountName, psw: psw }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Post, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/auth/login'), options)
            .toPromise()
            .then(function (res) {
            console.log(res.json());
            return res;
        })
            .then(function (res) { return res.json().result === 'Success'; })
            .catch(this.handleError);
    };
    DataService.prototype.getArticles = function (authors) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ headers: headers, withCredentials: true });
        return this.http.get(url('/articles/' + authors), options)
            .toPromise()
            .then(function (res) { return res.json(); })
            .then(function (res) { return res.posts; }) //  will they match??????
            .catch(this.handleError);
    };
    DataService.prototype.getFollowers = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ headers: headers, withCredentials: true });
        return this.http.get(url('/following'), options)
            .toPromise()
            .then(function (response) { return response.json().following; })
            .catch(this.handleError);
    };
    DataService.prototype.deleteFollower = function (user, follower) {
        var body = JSON.stringify({ accountName: user, follower: follower }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Delete, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/following'), options)
            .toPromise()
            .then(function (res) { return res.json().following; })
            .then(function (res) { return console.log(res); }) //  followers after deleting
            .catch(this.handleError);
    };
    DataService.prototype.addFollower = function (user, follower) {
        var body = JSON.stringify({ accountName: user, follower: follower }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/following'), options)
            .toPromise()
            .then(function (res) { return res.json().result; })
            .catch(this.handleError);
    };
    DataService.prototype.addPost = function (author, content, date, picture) {
        var body = JSON.stringify({ author: author, text: content, date: date, img: picture }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Post, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/articles'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
    };
    DataService.prototype.addComment = function (author, content, date, id) {
        var body = JSON.stringify({ author: author, text: content, date: date, id: id }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Post, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/articles/comment'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
    };
    DataService.prototype.editUser = function (accountName, phoneNumber, zip, email, psw, displayName) {
        var body = JSON.stringify({
            accountName: accountName,
            displayName: displayName,
            phoneNumber: phoneNumber,
            email: email,
            zip: zip
        }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        this.http.request(url('/profile/edit'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
        body = JSON.stringify({ accountName: accountName, psw: psw }); // Stringify payload
        headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        this.http.request(url('/auth/password'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
    };
    DataService.prototype.getUser = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ headers: headers, withCredentials: true });
        return this.http.get(url('/profile/user'), options)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    DataService.prototype.setStatus = function (accountName, headline) {
        var body = JSON.stringify({ accountName: accountName, headline: headline }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/profile/headline'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json().headline); })
            .catch(this.handleError);
    };
    DataService.prototype.getHeadline = function (accountNames) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ headers: headers, withCredentials: true });
        return this.http.get(url('/profile/headlines/' + accountNames), options)
            .toPromise()
            .then(function (response) { return response.json().headlines; })
            .catch(this.handleError);
    };
    DataService.prototype.getAvatar = function (accountNames) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ headers: headers, withCredentials: true });
        return this.http.get(url('/profile/avatars/' + accountNames), options)
            .toPromise()
            .then(function (response) { return response.json().avatars; })
            .catch(this.handleError);
    };
    DataService.prototype.updateAvatar = function (accountName, avatar) {
        var body = JSON.stringify({ accountName: accountName, avatar: avatar }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/profile/avatar'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
    };
    DataService.prototype.updateArticle = function (articleId, text) {
        var body = JSON.stringify({ text: text, id: articleId }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/articles/'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
    };
    DataService.prototype.updateComment = function (articleId, text, author, date) {
        var body = JSON.stringify({ text: text, id: articleId, author: author, date: date }); // Stringify payload
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestOptions */]({ method: __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestMethod */].Put, headers: headers, withCredentials: true, body: body }); // Create a request option
        return this.http.request(url('/articles/comment'), options)
            .toPromise()
            .then(function (res) { return console.log(res.json()); })
            .catch(this.handleError);
    };
    return DataService;
}());
DataService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], DataService);

var _a;
//# sourceMappingURL=data.service.js.map

/***/ }),

/***/ "../../../../../src/app/follower.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Follower; });
var Follower = (function () {
    function Follower(name, status, img) {
        this.name = name;
        this.status = status;
        this.img = img;
    }
    return Follower;
}());

//# sourceMappingURL=follower.js.map

/***/ }),

/***/ "../../../../../src/app/landing/landing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/landing/landing.component.html":
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n<div class=\"container\" style=\"width:25%\">\n  <nav>\n    <a routerLink=\"/login\" routerLinkActive=\"active\">Log in</a>\n    <a routerLink=\"/registration\" routerLinkActive=\"active\">Sign up</a>\n  </nav>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/landing/landing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LandingComponent = (function () {
    function LandingComponent() {
    }
    LandingComponent.prototype.ngOnInit = function () {
    };
    return LandingComponent;
}());
LandingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-landing',
        template: __webpack_require__("../../../../../src/app/landing/landing.component.html"),
        styles: [__webpack_require__("../../../../../src/app/landing/landing.component.css")]
    }),
    __metadata("design:paramtypes", [])
], LandingComponent);

//# sourceMappingURL=landing.component.js.map

/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"width:25%\">\n  <h2>Log in</h2>\n\n  <form [formGroup]=\"loginForm\" novalidate>\n    <div class=\"form-group\">\n      <input type=\"text\" class=\"form-control\"\n             #name\n             id=\"account\"\n             required\n             placeholder=\"Account Name\"\n             formControlName=\"name\">\n    </div>\n    <div class=\"form-group\">\n      <input type=\"password\" class=\"form-control\"\n             #psw\n             id=\"password\"\n             required\n             placeholder=\"Password\"\n             formControlName=\"password\">\n    </div>\n    <span *ngIf=\"display\">Wrong name or password</span><br><br>\n    <button [disabled]=\"!loginForm.valid\"\n            class=\"btn btn-default\"\n            (click) = \"login(name.value, psw.value)\"\n            id=\"loginBTN\">\n      Login\n    </button>\n  </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.loginForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required
            ]),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required
            ])
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.display = false;
    };
    LoginComponent.prototype.login = function (name, psw) {
        var _this = this;
        return this.dataService.loginUser(name, psw)
            .then(function (res) {
            if (res) {
                localStorage.setItem('userLogin', name);
                _this.router.navigate(['./main']);
            }
            else
                _this.display = true;
            return res;
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card {\n  margin: 0 auto;\n  width: 75%;\n  margin-bottom: 1.5pt;\n  padding: 3pt;\n  border: 1px solid #eeeeee;\n  border-radius: 10px;\n  min-height: 3.5em;\n  background-color:rgba(255,255,255,0.95);\n}\n.wrap {\n  min-width: 100%;\n  position: absolute;\n  background: #eff3f6 bottom;\n  min-height: 100%;\n  overflow: hidden;\n}\n.left{\n  margin: 0 auto;\n  margin-bottom: 1.5pt;\n  padding: 3pt;\n  border: 1px solid #eeeeee;\n  border-radius: 10px;\n  min-height: 3.5em;\n  background-color: white;\n  margin-left:10px;\n  position: absolute;\n  box-sizing: border-box;\n  width: 210px;\n  height: auto;\n}\n.logoDiv{\n  padding-top: 20px;\n  padding-bottom: 20px;\n  height: 70px;\n  background-color: #354457;\n  font-size: 17px;\n  color: #fff;\n  vertical-align: bottom;\n}\n.logoTitle{\n  margin-left:15px;\n  line-height: 1.7;\n}\n.menu-title {\n  margin-left:15px;\n  color: #828e9a;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  font-size: 14px;\n  font-weight: bold;\n}\n.menu-item {\n  padding-left:15px;\n  line-height: 40px;\n  height: 40px;\n  color: #aab1b7;\n  cursor: pointer;\n}\n.menu-item-active {\n  background-color: #3d4e60;\n  border-right: 4px solid #647f9d;\n  color: #fff;\n}\n.right{\n  box-sizing: border-box;\n  float: left;\n  box-sizing: border-box;\n  margin-left: 120px;\n  overflow-y: overlay;\n  overflow-x: hidden;\n  clear: both;\n  color: #717592;\n  min-width: 750px;\n  min-height: 500px;\n}\n.back{\n  background: url(https://upload.wikimedia.org/wikipedia/commons/d/db/Cijin_District_view_from_Mt_QiHou.jpg);\n  background-attachment:fixed;\n}\n\n\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"back\" style=\"padding-bottom: 50px\">\n  <div class=\"container\">\n    <div class=\"left\" style=\"margin-top: 20px; background-color:rgba(255,255,255,0.95)\">\n      <h1>Rice Book</h1>\n\n      <nav>\n        <a routerLink=\"/landing\" routerLinkActive=\"active\">Log out</a>\n        <a id=\"profileLink\" routerLink=\"/profile\" routerLinkActive=\"active\">Profile</a>\n      </nav>\n      <br>\n      <img src={{user.img}} height=\"192px\"/>\n      <br>\n      <p>Name: {{user.accountName}}</p>\n      <p id=\"headline\">Status: {{user.status}}</p>\n\n      <input id=\"headlineInput\" #status class=\"form-control\"/>\n      <button id=\"headlineBtn\" (click)=\"updateStatus(status.value); status.value=''\" class=\"btn btn-default\">\n        Update status\n      </button>\n\n      <h1>Friends</h1>\n      <div *ngFor=\"let follower of followers\">\n        <br>\n        <img src={{follower.img}} width=\"200px\">\n        <p id=\"follower\">Name: {{follower.name}}</p>\n        <span>Status: {{follower.status}}</span>\n        <br>\n        <button type=\"button\" class=\"btn btn-default\">Profile</button>\n        <button id=\"{{follower.name}}deleteBtn\" type=\"button\" class=\"btn btn-default\" (click)=\"deleteFollower(follower.name)\">Delete</button>\n      </div>\n      <div style=\"padding-top: 10px\">\n        <input id=\"followerInput\" #add class=\"form-control\"/>\n        <button id=\"followerBtn\" (click)=\"addFollower(add.value); add.value=''\" class=\"btn btn-default\">\n          Add friend\n        </button>\n        <br>\n        <span *ngIf=\"display\">The user doesn't exist</span><br><br>\n      </div>\n\n    </div>\n    <div class=\"container\" style=\"margin-top: 20px; min-height: 2000px\">\n      <div class=\"right\">\n\n        <div class=\"card\">\n          <textarea class=\"form-control\" rows=\"5\"\n                    id=\"inputArticle\"\n                    #textarea placeholder=\"Your post here\"></textarea>\n          <button (click)=\"textarea.value=''\" type=\"button\" class=\"btn btn-default\">cancel</button>\n          <button (click)=\"(post(textarea.value)); textarea.value=''\" id=\"postBTN\" type=\"button\"\n                  class=\"btn btn-default\">post\n          </button>\n          <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" accept=\"image/*;capture=camera\">\n          <button (click)=\"uploadImg()\">Upload</button>\n        </div>\n\n        <div class=\"card\" style=\"padding-top: 20px\">\n          <input id=\"searchInput\" #key class=\"form-control\"/>\n          <button id=\"searchBtn\" (click)=\"(filterArticles(key.value))\" class=\"btn btn-default\" style=\"align:right;\">\n            Search\n          </button>\n        </div>\n\n        <div id=\"articles\">\n          <ng-container *ngFor=\"let article of articles\" class=\"card\">\n            <div class=\"card\" *ngIf=\"article.display\" id=\"searchArticle\">\n              <span id=\"headerArticle{{article.id}}\">On {{article.date}}, {{article.author}} said:</span>\n              <br>\n              <img src={{article.img}} height=\"200px\">\n              <br>\n              <p id=\"{{article.id}}article\" contenteditable=\"false\">{{article.text}}</p>\n              <br>\n              <button type=\"button\" class=\"btn btn-default\" id=\"editArticle{{article.id}}\" (click)=\"editArticle(article.id)\">edit</button>\n              <button type=\"button\" class=\"btn btn-default\" id=\"{{article.id}}hideCommenting\"\n                      (click)=\"hideCommenting(article.id)\">comment</button>\n              <button type=\"button\" class=\"btn btn-default\" id=\"{{article.id}}hideComments\"\n                      (click)=\"hideComments(article.id)\">Show Comments\n              </button>\n              <div id=\"{{article.id}}commenting\" style=\"display: none\">\n                <textarea class=\"form-control\" rows=\"5\"\n                          id=\"inputComment\"\n                          #textarea placeholder=\"Your comment here\"></textarea>\n                <button (click)=\"textarea.value=''\" type=\"button\" class=\"btn btn-default\">cancel</button>\n                <button (click)=\"(comment(textarea.value, article.id)); textarea.value=''\" id=\"commentBtn\" type=\"button\"\n                        class=\"btn btn-default\">comment\n                </button>\n              </div>\n              <div id=\"{{article.id}}comments\" style=\"display: none\">\n                <div *ngFor=\"let comment of article.comments\" class=\"card\">\n                  <p class=\"comments\">Date: {{comment.date}}</p>\n                  <label>Content: </label><span class=\"comments\" id=\"{{comment.date.toString()}}comment{{article.id}}\" contenteditable=\"false\">{{comment.text}}</span>\n                  <p class=\"comments\">Comment By: {{comment.author}}</p>\n                  <button type=\"button\" class=\"btn btn-default\" id=\"{{article.id}}editComment{{comment.date}}\" (click)=\"editComment(article.id, comment.date)\">edit</button>\n                </div>\n              </div>\n            </div>\n          </ng-container>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <router-outlet></router-outlet>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__article__ = __webpack_require__("../../../../../src/app/article.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comment__ = __webpack_require__("../../../../../src/app/comment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user__ = __webpack_require__("../../../../../src/app/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_cloudinary__ = __webpack_require__("../../../../ng2-cloudinary/dist/esm/src/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__follower__ = __webpack_require__("../../../../../src/app/follower.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MainComponent = (function () {
    function MainComponent(dataService) {
        this.dataService = dataService;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_5_ng2_cloudinary__["b" /* CloudinaryUploader */](new __WEBPACK_IMPORTED_MODULE_5_ng2_cloudinary__["a" /* CloudinaryOptions */]({ cloudName: 'yw68', uploadPreset: 'agfevxpi' }));
    }
    MainComponent.prototype.getUser = function () {
        var _this = this;
        return this.dataService
            .getUser()
            .then(function (res) {
            _this.user.img = res.avatar;
            _this.user.status = res.headline;
            _this.user.accountName = _this.userLogin;
        });
    };
    MainComponent.prototype.getArticles = function () {
        var _this = this;
        var temp = this.userLogin;
        this.followersName.forEach(function (e) { return temp += (',' + e); });
        return this.dataService
            .getArticles(temp)
            .then(function (articles) { return _this.articles = articles; });
    };
    MainComponent.prototype.getFollowers = function () {
        var _this = this;
        return this.dataService
            .getFollowers()
            .then(function (res) {
            _this.followers = [];
            _this.followersName = res;
            res.forEach(function (e) { return _this.followers.push(new __WEBPACK_IMPORTED_MODULE_6__follower__["a" /* Follower */](e, '', '')); });
            return res;
        })
            .then(function (_) {
            var temp = _this.userLogin;
            _this.followersName.forEach(function (e) { return temp += (',' + e); });
            return _this.dataService
                .getHeadline(temp)
                .then(function (headlines) { return _this.followers.forEach(function (follower) { return follower.status = headlines.find(function (headline) { return headline.username === follower.name; }).headline; }); });
        })
            .then(function (_) {
            var temp = _this.userLogin;
            _this.followersName.forEach(function (e) { return temp += (',' + e); });
            return _this.dataService
                .getAvatar(temp)
                .then(function (avatars) { return _this.followers.forEach(function (follower) { return follower.img = avatars.find(function (avatar) { return avatar.username === follower.name; }).avatar; }); });
        })
            .then(function (_) { return console.log(_this.followers); });
    };
    MainComponent.prototype.filterArticles = function (inputKey) {
        if (inputKey != null)
            this.articles.forEach(function (element) {
                if (element.author.indexOf(inputKey) === -1 && element.text.indexOf(inputKey) === -1)
                    element.display = false;
            });
        if (inputKey === '')
            this.articles.forEach(function (x) { return x.display = true; });
    };
    MainComponent.prototype.deleteFollower = function (follower) {
        console.log(follower);
        this.followers = this.followers.filter(function (e) { return e.name !== follower; });
        this.articles = this.articles.filter(function (e) { return e.author !== follower; });
        this.dataService.deleteFollower(this.userLogin, follower);
    };
    MainComponent.prototype.addFollower = function (follower) {
        var _this = this;
        this.dataService
            .addFollower(this.userLogin, follower)
            .then(function (res) {
            console.log(res);
            _this.display = (res !== 'Success');
            return res;
        })
            .then(function (_) {
            if (!_this.display) {
                _this.getFollowers()
                    .then(function () { return _this.getArticles(); })
                    .then(function () { return console.log(_this.articles); })
                    .then(function () { return _this.articles.forEach(function (e) { return e.display = true; }); });
            }
            console.log(_this.followers);
        });
    };
    MainComponent.prototype.post = function (inputText) {
        var _this = this;
        var date = new Date();
        this.dataService
            .addPost(this.userLogin, inputText, date.toDateString(), this.imgPost)
            .then(function () {
            _this.articles.unshift(new __WEBPACK_IMPORTED_MODULE_1__article__["a" /* Article */](inputText, date, _this.imgPost, [], _this.userLogin));
            _this.imgPost = '';
        });
    };
    MainComponent.prototype.uploadImg = function () {
        this.uploader.uploadAll();
    };
    MainComponent.prototype.updateStatus = function (inputStatus) {
        var _this = this;
        this.dataService.setStatus(this.userLogin, inputStatus)
            .then(function (_) { return _this.user.status = inputStatus; });
    };
    MainComponent.prototype.hideComments = function (articleId) {
        var comments = document.getElementById(articleId + 'comments');
        var button = document.getElementById(articleId + 'hideComments');
        if (comments.style.display === 'none') {
            button.innerHTML = 'Hide Comments';
            comments.style.display = 'block';
        }
        else {
            button.innerHTML = 'Show Comments';
            comments.style.display = 'none';
        }
    };
    MainComponent.prototype.hideCommenting = function (articleId) {
        var comments = document.getElementById(articleId + 'commenting');
        var button = document.getElementById(articleId + 'hideCommenting');
        if (comments.style.display === 'none') {
            button.innerHTML = 'Reveal Comments';
            comments.style.display = 'block';
        }
        else {
            button.innerHTML = 'Comments';
            comments.style.display = 'none';
        }
    };
    MainComponent.prototype.comment = function (inputText, id) {
        var date = new Date();
        var author = this.articles.find(function (e) { return e.id === id; }).author;
        var newComment = new __WEBPACK_IMPORTED_MODULE_2__comment__["a" /* Comment */](inputText, date, author);
        this.articles.find(function (e) { return e.id === id; }).comments.unshift(newComment);
        this.dataService
            .addComment(this.userLogin, inputText, date, id);
    };
    MainComponent.prototype.editArticle = function (articleId) {
        var article = document.getElementById(articleId + 'article');
        var button = document.getElementById('editArticle' + articleId);
        var curArticle = this.articles.filter(function (res) { return res.id === articleId; });
        if (curArticle[0].author === this.userLogin) {
            if (button.innerHTML === 'edit') {
                article.contentEditable = 'true';
                button.innerHTML = 'commit';
            }
            else {
                curArticle[0].text = article.innerHTML;
                button.innerHTML = 'edit';
                this.dataService.updateArticle(articleId, article.innerHTML);
                article.contentEditable = 'false';
            }
        }
        else {
            article.contentEditable = 'false';
        }
    };
    MainComponent.prototype.editComment = function (articleId, comDate) {
        var comment = document.getElementById(comDate + 'comment' + articleId);
        var button = document.getElementById(articleId + 'editComment' + comDate);
        var curArticle = this.articles.filter(function (res) { return res.id === articleId; });
        var curComment = curArticle[0].comments.filter(function (res) { return res.date === comDate; });
        if (curComment[0].author === this.userLogin) {
            if (button.innerHTML === 'edit') {
                comment.contentEditable = 'true';
                button.innerHTML = 'commit';
            }
            else {
                curComment[0].text = comment.innerHTML;
                button.innerHTML = 'edit';
                comment.contentEditable = 'false';
                this.dataService.updateComment(articleId, comment.innerHTML, curComment[0].author, comDate);
            }
        }
        else {
            comment.contentEditable = 'false';
        }
    };
    MainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.uploader.onSuccessItem = function (item, response, status, headers) {
            var res = JSON.parse(response);
            _this.imgPost = res.url;
            return { item: item, response: response, status: status, headers: headers };
        };
        this.articles = [];
        this.imgPost = '';
        this.display = false;
        this.userLogin = localStorage.getItem('userLogin');
        this.user = new __WEBPACK_IMPORTED_MODULE_3__user__["a" /* User */](0, '', '', '', '', '', '', null, '', '', [], [], '');
        this.followers = [];
        this.getUser()
            .then(function (_) { return _this.getFollowers(); })
            .then(function (_) { return _this.getArticles(); })
            .then(function (_) { return _this.articles.forEach(function (e) { return e.display = true; }); })
            .then(function (_) { return console.log(_this.articles); });
    };
    return MainComponent;
}());
MainComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-main',
        template: __webpack_require__("../../../../../src/app/main/main.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/main.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__data_service__["a" /* DataService */]) === "function" && _a || Object])
], MainComponent);

var _a;
//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".left{\n  margin-left:10px;\n  position: absolute;\n  box-sizing: border-box;\n  width: 400px;\n  height: 100%;\n}\n.right{\n  box-sizing: border-box;\n  float: left;\n  padding-left: 400px;\n  overflow-y: overlay;\n  overflow-x: hidden;\n  clear: both;\n  min-width: 100%;\n  min-height: 500px;\n}\n.ng-valid[required], .ng-valid.required  {\n  border-left: 5px solid #42A948; /* green */\n}\n\n.ng-invalid:not(form)  {\n  border-left: 5px solid #a94442; /* red */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"left\">\n    <nav>\n      <a routerLink=\"/main\" routerLinkActive=\"active\">Main page</a>\n    </nav>\n    <router-outlet></router-outlet>\n    <div style=\"padding-top: 20px; width:210px; padding-left: 5px; border:1px solid #eeeeee; border-radius:10px;\">\n      <h2>Current Info</h2>\n      <img src={{model.img}} height=\"192px\">\n      <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" accept=\"image/*;capture=camera\">\n      <button (click)=\"uploadAvatar()\">Upload</button>\n      <div class=\"form-group\">\n        <label>Display Name:</label>\n        <p>{{model.displayName}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Email Address:</label>\n        <p id=\"displayEmail\">{{model.email}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Phone Number:</label>\n        <p>{{model.phoneNumber}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Date of Birth:</label>\n        <p>{{model.dateOfBirth}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Zip Code:</label>\n        <p id=\"displayZip\">{{model.zip}}</p>\n      </div>\n    </div>\n  </div>\n  <div class=\"right\">\n    <br><br>\n    <div\n      style=\"padding-top: 20px; width:500px; padding-left: 20px; padding-bottom: 20px; padding-right: 20px; border:1px solid #eeeeee; border-radius:10px;\">\n      <h2>Update Info</h2>\n      <form #userForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n\n        <div class=\"form-group\">\n          <label for=\"display\">Display Name</label>\n          <input type=\"text\" class=\"form-control\" id=\"display\"\n                 [(ngModel)]=\"current.displayName\" name=\"display\">\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"email\">Email</label>\n          <input type=\"text\" class=\"form-control\" id=\"email\" required\n                 [(ngModel)]=\"current.email\" name=\"email\"\n                 pattern=\"^[a-zA-Z0–9_.+-]+@[a-zA-Z0–9-]+.[a-zA-Z0–9.-]+$\"\n                 #email=\"ngModel\">\n          <div [hidden]=\"email.valid || email.pristine\"\n               class=\"alert alert-danger\">\n            Email is required and format should be <i>john@domain.com</i>.\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"phone\">Phone</label>\n          <input type=\"text\" class=\"form-control\" id=\"phone\" required\n                 [(ngModel)]=\"current.phoneNumber\" name=\"phone\"\n                 pattern=\"^\\d\\d\\d-\\d\\d\\d-\\d\\d\\d\\d$\"\n                 #phone=\"ngModel\">\n          <div [hidden]=\"phone.valid || phone.pristine\"\n               class=\"alert alert-danger\">\n            Phone Number is required and format should be <i>123-456-7890</i>.\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"zip\">Zip Code</label>\n          <input type=\"number\" class=\"form-control\" id=\"zip\" required\n                 [(ngModel)]=\"current.zip\" name=\"zip\"\n                 pattern=\"^\\d\\d\\d\\d\\d$\"\n                 #zip=\"ngModel\">\n          <div [hidden]=\"zip.valid || zip.pristine\"\n               class=\"alert alert-danger\">\n            Zip Code is required and consists of five integers\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label>Password</label>\n          <input id=\"password\" type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"current.psw\"\n                 required #password=\"ngModel\" validateEqual=\"confirmPassword\" reverse=\"true\">\n          <div [hidden]=\"password.valid || (password.pristine)\">\n            Password is required\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label>Retype password</label>\n          <input id=\"repassword\" type=\"password\" class=\"form-control\" name=\"confirmPassword\" [(ngModel)]=\"current.repsw\"\n                 required validateEqual=\"password\" #confirmPassword=\"ngModel\">\n          <div [hidden]=\"confirmPassword.valid ||  (confirmPassword.pristine)\">\n            Password mismatch\n          </div>\n        </div>\n        <button id=\"submitBtn\" type=\"submit\" class=\"btn btn-success\" [disabled]=\"!userForm.form.valid\">\n          Submit\n        </button>\n      </form>\n    </div>\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_cloudinary__ = __webpack_require__("../../../../ng2-cloudinary/dist/esm/src/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfileComponent = (function () {
    function ProfileComponent(dataService) {
        this.dataService = dataService;
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_cloudinary__["b" /* CloudinaryUploader */](new __WEBPACK_IMPORTED_MODULE_3_ng2_cloudinary__["a" /* CloudinaryOptions */]({ cloudName: 'yw68', uploadPreset: 'agfevxpi' }));
    }
    ProfileComponent.prototype.uploadAvatar = function () {
        this.uploader.uploadAll();
    };
    ProfileComponent.prototype.onSubmit = function () {
        this.dataService.editUser(this.userLogin, this.current.phoneNumber, this.current.zip, this.current.email, this.current.psw, this.current.displayName);
        this.model.displayName = this.current.displayName;
        this.model.phoneNumber = this.current.phoneNumber;
        this.model.zip = this.current.zip;
        this.model.email = this.current.email;
        this.model.psw = this.current.psw;
        this.model.repsw = this.current.repsw;
        this.current = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', null, '', '', [], [], '');
    };
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.uploader.onSuccessItem = function (item, response, status, headers) {
            var res = JSON.parse(response);
            _this.current.img = res.url;
            _this.model.img = res.url;
            console.log(_this.current.img);
            _this.dataService.updateAvatar(localStorage.getItem('userLogin'), _this.model.img);
            console.log('12345');
            return { item: item, response: response, status: status, headers: headers };
        };
        this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', null, '', '', [], [], '');
        this.current = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', null, '', '', [], [], '');
        this.userLogin = localStorage.getItem('userLogin');
        this.dataService.getUser().then(function (user) {
            _this.model.displayName = user.displayName;
            _this.model.email = user.email;
            _this.model.dateOfBirth = user.dob.slice(0, 10);
            _this.model.zip = user.zip;
            _this.model.phoneNumber = user.phoneNumber;
            console.log(_this.model);
        });
        this.dataService.getAvatar('').then(function (res) { return _this.model.img = res; });
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-profile',
        template: __webpack_require__("../../../../../src/app/profile/profile.component.html"),
        styles: [__webpack_require__("../../../../../src/app/profile/profile.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object])
], ProfileComponent);

var _a;
//# sourceMappingURL=profile.component.js.map

/***/ }),

/***/ "../../../../../src/app/registration/registration.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ng-valid[required], .ng-valid.required  {\n  border-left: 5px solid #42A948; /* green */\n}\n\n.ng-invalid:not(form)  {\n  border-left: 5px solid #a94442; /* red */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/registration/registration.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"width:25%\">\n  <h2>Registration</h2>\n\n  <form #userForm=\"ngForm\">\n    <div class=\"form-group\">\n      <label for=\"account\">Account Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"account\" required\n             [(ngModel)]=\"model.accountName\" name=\"account\"\n             pattern=\"^[a-zA-Z][a-zA-Z0-9]+$\"\n             #account=\"ngModel\">\n      <div [hidden]=\"account.valid || account.pristine\"\n           class=\"alert alert-danger\">\n        Account Name is required and it can only start with letter.\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"display\">Display Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"display\"\n             [(ngModel)]=\"model.displayName\" name=\"display\">\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"email\">Email</label>\n      <input type=\"text\" class=\"form-control\" id=\"email\" required\n             [(ngModel)]=\"model.email\" name=\"email\"\n             pattern=\"^[a-zA-Z0–9_.+-]+@[a-zA-Z0–9-]+.[a-zA-Z0–9.-]+$\"\n             #email=\"ngModel\">\n      <div [hidden]=\"email.valid || email.pristine\"\n           class=\"alert alert-danger\">\n        Email is required and format should be <i>john@domain.com</i>.\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"phone\">Phone</label>\n      <input type=\"text\" class=\"form-control\" id=\"phone\" required\n             [(ngModel)]=\"model.phoneNumber\" name=\"phone\"\n             pattern=\"^\\d\\d\\d-\\d\\d\\d-\\d\\d\\d\\d$\"\n             #phone=\"ngModel\">\n      <div [hidden]=\"phone.valid || phone.pristine\"\n           class=\"alert alert-danger\">\n        Phone Number is required and format should be <i>123-456-7890</i>.\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"birthday\">Date of Birth</label>\n      <input type=\"date\" class=\"form-control\" id=\"birthday\" required\n             [(ngModel)]=\"model.dateOfBirth\" name=\"birthday\"\n             #birthday=\"ngModel\" validateAge=\"birthday\">\n      <div [hidden]=\"birthday.valid || birthday.pristine\"\n           class=\"alert alert-danger\">\n        Sorry, your age is under 18\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"zip\">Zip Code</label>\n      <input type=\"number\" class=\"form-control\" id=\"zip\" required\n             [(ngModel)]=\"model.zip\" name=\"zip\"\n             pattern=\"^\\d\\d\\d\\d\\d$\"\n             #zip=\"ngModel\">\n      <div [hidden]=\"zip.valid || zip.pristine\"\n           class=\"alert alert-danger\">\n        Zip Code is required and consists of five integers\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label>Password</label>\n      <input type=\"password\" class=\"form-control\" id=\"password\"\n             name=\"password\" [(ngModel)]=\"model.psw\"\n             required #password=\"ngModel\" validateEqual=\"confirmPassword\" reverse=\"true\">\n      <div [hidden]=\"password.valid || (password.pristine)\">\n        Password is required\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label>Retype password</label>\n      <input type=\"password\" class=\"form-control\" id=\"repassword\"\n             name=\"confirmPassword\" [(ngModel)]=\"model.repsw\"\n             required validateEqual=\"password\" #confirmPassword=\"ngModel\">\n      <div [hidden]=\"confirmPassword.valid ||  (confirmPassword.pristine)\">\n        Password mismatch\n      </div>\n    </div>\n\n    <button type=\"submit\" class=\"btn btn-success\"\n            [disabled]=\"!userForm.form.valid\"\n            (click)=\"register()\"\n            id=\"registrationBTN\"\n            routerLink=\"/login\" routerLinkActive=\"active\">\n      Submit\n    </button>\n    <button type=\"button\" class=\"btn btn-default\"\n            (click)=\"userForm.reset()\">\n      Reset\n    </button>\n\n  </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/registration/registration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegistrationComponent = (function () {
    function RegistrationComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', null, '', '', [], [], '');
    };
    RegistrationComponent.prototype.register = function () {
        console.log(this.model);
        this.dataService.addUser(this.model.accountName, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.dateOfBirth, this.model.displayName);
        this.router.navigate(['./login']);
    };
    return RegistrationComponent;
}());
RegistrationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-registration',
        template: __webpack_require__("../../../../../src/app/registration/registration.component.html"),
        styles: [__webpack_require__("../../../../../src/app/registration/registration.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], RegistrationComponent);

var _a, _b;
//# sourceMappingURL=registration.component.js.map

/***/ }),

/***/ "../../../../../src/app/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, displayName) {
        this.id = id;
        this.accountName = accountName;
        this.phoneNumber = phoneNumber;
        this.zip = zip;
        this.email = email;
        this.psw = psw;
        this.repsw = repsw;
        this.dateOfBirth = dateOfBirth;
        this.status = status;
        this.img = img;
        this.following = following;
        this.articles = articles;
        this.displayName = displayName;
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ "../../../../../src/app/validator/age-validator.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgeValidator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AgeValidator = AgeValidator_1 = (function () {
    function AgeValidator(validateEqual) {
        this.validateEqual = validateEqual;
    }
    AgeValidator.prototype.validate = function (c) {
        // self value
        if (c.value == null) {
            return {
                validateEqual: false
            };
        }
        var v = parseInt(c.value.slice(0, 4), 10);
        if (new Date().getFullYear() - v < 18) {
            return {
                validateEqual: false
            };
        }
        return null;
    };
    return AgeValidator;
}());
AgeValidator = AgeValidator_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[validateAge][formControlName],[validateAge][formControl],[validateAge][ngModel]',
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* NG_VALIDATORS */], useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return AgeValidator_1; }), multi: true }
        ]
    }),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Attribute"])('validateAge')),
    __metadata("design:paramtypes", [String])
], AgeValidator);

var AgeValidator_1;
//# sourceMappingURL=age-validator.directive.js.map

/***/ }),

/***/ "../../../../../src/app/validator/equal-validator.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EqualValidator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var EqualValidator = EqualValidator_1 = (function () {
    function EqualValidator(validateEqual, reverse) {
        this.validateEqual = validateEqual;
        this.reverse = reverse;
    }
    Object.defineProperty(EqualValidator.prototype, "isReverse", {
        get: function () {
            if (!this.reverse)
                return false;
            return this.reverse === 'true' ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    EqualValidator.prototype.validate = function (c) {
        // self value
        var v = c.value;
        // control vlaue
        var e = c.root.get(this.validateEqual);
        // value not equal
        if (e && v !== e.value && !this.isReverse) {
            return {
                validateEqual: false
            };
        }
        // value equal and reverse
        if (e && v === e.value && this.isReverse) {
            delete e.errors['validateEqual'];
            if (!Object.keys(e.errors).length)
                e.setErrors(null);
        }
        // value not equal and reverse
        if (e && v !== e.value && this.isReverse) {
            e.setErrors({ validateEqual: false });
        }
        return null;
    };
    return EqualValidator;
}());
EqualValidator = EqualValidator_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* NG_VALIDATORS */], useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return EqualValidator_1; }), multi: true }
        ]
    }),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Attribute"])('validateEqual')),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Attribute"])('reverse')),
    __metadata("design:paramtypes", [String, String])
], EqualValidator);

var EqualValidator_1;
//# sourceMappingURL=equal-validator.directive.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map