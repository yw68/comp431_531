import { Component, OnInit } from '@angular/core';


import { Article } from "../article";
import { Comment } from "../comment";
import { User } from "../user";
import { DataService } from '../data.service';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { Follower } from "../follower";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  user: User;
  articles: Article[];
  followers: Follower[];
  followersName: string[];
  userLogin: string;
  display: boolean;
  imgPost: string;
  uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions({ cloudName: 'yw68', uploadPreset: 'agfevxpi' })
  );

  constructor(private dataService: DataService) {
  }

  getUser() {
    return this.dataService
      .getUser()
      .then(res => {
        this.user.img = res.avatar;
        this.user.status = res.headline;
        this.user.accountName = this.userLogin;
      });
  }
  getArticles() {
    let temp = this.userLogin;
    this.followersName.forEach(e => temp += (',' + e));
    return this.dataService
      .getArticles(temp)
      .then(articles => this.articles = articles);
  }
  getFollowers() {
    return this.dataService
      .getFollowers()
      .then(res => {
        this.followers = [];
        this.followersName = res;
        res.forEach(e => this.followers.push(new Follower(e, '', '')));
        return res;
      })
      .then(_ => {
        let temp = this.userLogin;
        this.followersName.forEach(e => temp += (',' + e));
        return this.dataService
          .getHeadline(temp)
          .then(headlines => this.followers.forEach(follower => follower.status = headlines.find(headline => headline.username === follower.name).headline));
      })
      .then(_ => {
        let temp = this.userLogin;
        this.followersName.forEach(e => temp += (',' + e));
        return this.dataService
          .getAvatar(temp)
          .then(avatars => this.followers.forEach(follower => follower.img = avatars.find(avatar => avatar.username === follower.name).avatar));
      })
      .then(_ => console.log(this.followers));
  }
  filterArticles(inputKey: string): void {
    if (inputKey != null) this.articles.forEach(function(element) {
      if (element.author.indexOf(inputKey) === -1 && element.text.indexOf(inputKey) === -1) element.display = false;
    });
    if (inputKey === '') this.articles.forEach(x => x.display = true);
  }
  deleteFollower(follower: string): void {
    console.log(follower);
    this.followers = this.followers.filter(e => e.name !== follower);
    this.articles = this.articles.filter(e => e.author !== follower);
    this.dataService.deleteFollower(this.userLogin, follower);
  }
  addFollower(follower: string): void {
    this.dataService
      .addFollower(this.userLogin, follower)
      .then(res => {
        console.log(res);
        this.display = (res !== 'Success');
        return res;
      })
      .then(_ => {
        if (!this.display) {
          this.getFollowers()
            .then(() => this.getArticles())
            .then(() => console.log(this.articles))
            .then(() => this.articles.forEach(e => e.display = true))
          ;
        }
        console.log(this.followers);
      });
  }
  post(inputText: string): void {
    const date = new Date();
    this.dataService
      .addPost(this.userLogin, inputText, date.toDateString(), this.imgPost)
      .then(() => {
        this.articles.unshift(new Article(inputText, date, this.imgPost, [], this.userLogin));
        this.imgPost = '';
      });
  }
  uploadImg() {
    this.uploader.uploadAll();
  }
  updateStatus(inputStatus: string): void {
    this.dataService.setStatus(this.userLogin, inputStatus)
      .then(_ => this.user.status = inputStatus);
  }
  hideComments(articleId) {
    const comments = document.getElementById(articleId + 'comments');
    const button = document.getElementById (articleId + 'hideComments');
    if (comments.style.display === 'none') {
      button.innerHTML = 'Hide Comments';
      comments.style.display = 'block';
    } else {
      button.innerHTML = 'Show Comments';
      comments.style.display = 'none';
    }
  }
  hideCommenting(articleId) {
    const comments = document.getElementById(articleId + 'commenting');
    const button = document.getElementById (articleId + 'hideCommenting');
    if (comments.style.display === 'none') {
      button.innerHTML = 'Reveal Comments';
      comments.style.display = 'block';
    } else {
      button.innerHTML = 'Comments';
      comments.style.display = 'none';
    }
  }
  comment(inputText: string, id: string): void {
    const date = new Date();
    const author = this.articles.find(e => e.id === id).author;
    const newComment = new Comment(inputText, date, author);
    this.articles.find(e => e.id === id).comments.unshift(newComment);
    this.dataService
      .addComment(this.userLogin, inputText, date, id);
  }
  editArticle(articleId) {
    const article = document.getElementById(articleId + 'article');
    const button = document.getElementById('editArticle' + articleId);
    const curArticle = this.articles.filter(res => res.id === articleId);
    if (curArticle[0].author === this.userLogin) {
      if (button.innerHTML === 'edit') {
        article.contentEditable = 'true';
        button.innerHTML = 'commit';
      } else {
        curArticle[0].text = article.innerHTML;
        button.innerHTML = 'edit';
        this.dataService.updateArticle(articleId, article.innerHTML);
        article.contentEditable = 'false';
      }
    } else {
      article.contentEditable = 'false';
    }
  }
  editComment(articleId: string, comDate) {
    const comment = document.getElementById(comDate + 'comment' + articleId);
    const button = document.getElementById(articleId + 'editComment' + comDate);
    const curArticle = this.articles.filter(res => res.id === articleId);
    const curComment = curArticle[0].comments.filter(res => res.date === comDate);
    if (curComment[0].author === this.userLogin) {
      if (button.innerHTML === 'edit') {
        comment.contentEditable = 'true';
        button.innerHTML = 'commit';
      } else {
        curComment[0].text = comment.innerHTML;
        button.innerHTML = 'edit';
        comment.contentEditable = 'false';
        this.dataService.updateComment(articleId, comment.innerHTML, curComment[0].author, comDate);
      }
    } else {
      comment.contentEditable = 'false';
    }

  }
  ngOnInit() {
    this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any): any => {
      const res: any = JSON.parse(response);
      this.imgPost = res.url;
      return { item, response, status, headers };
    };
    this.articles = [];
    this.imgPost = '';
    this.display = false;
    this.userLogin = localStorage.getItem('userLogin');
    this.user = new User(0, '', '', '', '', '', '', null, '', '', [], [], '');
    this.followers = [];
    this.getUser()
      .then(_ => this.getFollowers())
      .then(_ => this.getArticles())
      .then(_ => this.articles.forEach(e => e.display = true))
      .then(_ => console.log(this.articles));
  }
}
