import { url, resource } from './resource';
export const fetchArticles = () => {
  return resource('GET', 'articles', {});
};
export const searchKey = (newSearchKey) => newSearchKey;
export const filterArticles = (key, articles) => {
  return articles.filter(e => {
    return e.author === key || e.content === key;
  });
};
