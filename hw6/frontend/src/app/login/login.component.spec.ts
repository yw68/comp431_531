import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RouterTestingModule} from "@angular/router/testing";
import {login, logout} from '../authentication';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { url, resource } from '../resource';
import { DataService } from "../data.service";
import 'fetch-mock';
import {HttpModule} from "@angular/http";
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ReactiveFormsModule, HttpModule, RouterTestingModule],
      providers: [DataService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const fetchMock = require('fetch-mock');
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should log in a user', (done) => {
    const name = 'yw';
    const psw = '111-111-111'
    fetchMock.mock(`${url}/login`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      json: { status: 'Success' }
    });
    login(name, psw)
      .then(res => expect(res.status).toEqual('Success'))
      .then(done)
      .catch(done);
  });
  it('should not log in an invalid user', (done) => {
    const name = 'yw';
    const psw = '222222222'
    fetchMock.mock(`${url}/login`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      json: { status: 'Fail' }
    });
    login(name, psw)
      .then(res => expect(res.status).toEqual('Fail'))
      .then(done)
      .catch(done);
  });
  it('should log out a user (state should be cleared)', (done) => {
    fetchMock.mock(`${url}/logout`, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      json: { status: 'Logout' }
    });
    logout()
      .then(res => expect(res.status).toEqual('Logout'))
      .then(done)
      .catch(done);
  });
  it('should update error message (for displaying error mesage to user)', () => {
    component.login('ywwwww', '111111111')
      .then(res => expect(res).toEqual(true));
  });
  it('should update success message (for displaying success message to user)', () => {
    component.login('yw68', '111-111-111')
      .then(res => expect(res).toEqual(false));
  });
});
