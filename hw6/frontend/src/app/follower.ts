export class Follower {
  name: string;
  status: string;
  img: string;

  constructor(name: string,
              status: string,
              img: string) {
    this.name = name;
    this.status = status;
    this.img = img;
  }
}
