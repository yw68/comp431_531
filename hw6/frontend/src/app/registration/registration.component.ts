import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { DataService } from "../data.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  model: User;
  constructor(private dataService: DataService, private router: Router) { }
  ngOnInit() {
    this.model = new User(0, '', '', '', '', '', '', null, '', '', [], [], '');
  }
  register() {
    console.log(this.model);
    this.dataService.addUser(this.model.accountName, this.model.phoneNumber, this.model.zip, this.model.email, this.model.psw, this.model.dateOfBirth, this.model.displayName);
    this.router.navigate(['./login']);
  }
}
