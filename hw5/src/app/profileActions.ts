import { url, resource } from './resource';
export const fetchInfo = () => {
  return resource('GET', 'profile', {});
};
export const update = (content) => {
  return resource('POST', 'profile', { headline: content });
};
