import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Article } from "./article";
import { User } from "./user";
import {forEach} from "@angular/router/src/utils/collection";
import {delay} from "q";


@Injectable()
export class DataService {

  private dataUrl = 'api/data';

  private handleError(error: any): Promise<any> {
    console.error('An HTTP error occurred', error);
    return Promise.reject(error.message || error);
  }
  constructor(private http: Http) { }

  getArticles(id: number): Promise<Article[]> {
    // console.log(localStorage.getItem('data'));
    let owners = new Array();
    let t;
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(res => {
        console.log(res);
        return res;
      })
      .then(res => {
        console.log(id);
        t = res.json();
        return this.getUsers();

      })
      .then(users => {
        owners.push(users.find(p => p.id === id).accountName);
        console.log(id);
        console.log(owners);
        const temp = users.find(p => p.id === id).following;
        console.log(temp);
        for (let i = 0; i < temp.length; i++) {
          owners.push(users.find(user => user.id === temp[i]).accountName);
        }
        console.log(owners);
      })
      .then(_ => t.articles as Article[])
      .then(articles => {
        console.log(owners);
        return articles.filter(article => owners.includes(article.author));
      })
      .catch(this.handleError);
  }
  getAllArticles(): Promise<Article[]> {
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(response => response.json().articles as Article[])
      .catch(this.handleError);
  }
  getUsers(): Promise<User[]> {
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(response => response.json().users as User[])
      .catch(this.handleError);
  }
  deleteFollower(user: number, follower: number): Promise<number[]> {
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(res => res.json().users as User[])
      .then(users => {
        users.find(e => e.id === user).following = users.find(e => e.id === user).following.filter(e => e !== follower)
        localStorage.setItem('users', JSON.stringify(users));
      })
      .catch(this.handleError);
  }
  addFollower(user: number, follower: number): Promise<number> {
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(res => res.json().users as User[])
      .then(users => {
        users.find(e => e.id === user).following.push(follower);
        localStorage.setItem('users', JSON.stringify(users));
      })
      .catch(this.handleError);
  }
  refreshFollower(articles, follow, input) {
    follow.push(JSON.parse(localStorage.getItem('users')).find(e => e.accountName === input));
    this.getAllArticles()
      .then(res => articles.push(res.find(e => e.author === input)));
    return {follow, articles};
  }
  refreshFollower1(articles, follow, input) {
    const x1 = follow.filter(e => e.accountName !== input);
    const x2 = articles.filter(e => e.author !== input);
    return {follow: x1, articles: x2};
  }
  addUser(id: number, accountName: string, phoneNumber: string, zip: string, email: string, psw: string, repsw: string, dateOfBirth: string, status: string, img: string, following: number[], articles: number[], display: boolean, displayName?: string): void {
    const u = new User(id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, display, displayName);
    this.http.get(this.dataUrl)
      .toPromise()
      .then(res => res.json().users as User[])
      .then(users => users.push(u))
      .then(newUsers => localStorage.setItem('users', JSON.stringify(newUsers)))
      .catch(this.handleError);
  }
  // loginUser(userName: string, psw: string): Promise<boolean> {
  //   return this.http.get(this.dataUrl)
  //     .toPromise()
  //     .then(res => res.json().users as User[])
  //     .then(res => res.find(e => e.accountName === userName))
  //     .then(res => res.psw === psw)
  //     .catch(this.handleError);
  // }
  addPost(id: number, content: string): Promise<number> {
    let index = 0;
    let name = '';
    let articles;
    return this.http.get(this.dataUrl)
      .toPromise()
      .then(res => {
        name = res.json().users.find(e => e.id === id).accountName;
        index = res.json().articles[0].id + 1;
        articles = res.json().articles;
        articles.unshift(new Article(index, content, new Date().toString(), '', [], name, true));
        localStorage.setItem('articles', JSON.stringify(articles));
        return res;
      })
      .then(res => res.json().users as User[])
      .then(res => res.find(e => e.id === id).articles.push(index))
      .catch(this.handleError);
  }
  editUser(phoneNumber: string, zip: string, email: string, psw: string, repsw: string, dateOfBirth: string, displayName?: string): void {
    this.http.get(this.dataUrl)
      .toPromise()
      .then(res => res.json().users as User[])
      .then(usrs => {
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).phoneNumber = phoneNumber;
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).zip = zip;
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).email = email;
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).psw = psw;
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).repsw = repsw;
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).dateOfBirth = dateOfBirth;
        usrs.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).displayName = displayName;
        localStorage.setItem('users', JSON.stringify(usrs));
      });
  }
  setStatus(input: string) {
    const t = JSON.parse(localStorage.getItem('users'));
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).status = input;
    localStorage.setItem('users', JSON.stringify(t));
  }
  saveInfo(user: User) {
    const t = JSON.parse(localStorage.getItem('users'));
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).displayName = user.displayName;
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).email = user.email;
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).phoneNumber = user.phoneNumber;
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).zip = user.zip;
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).psw = user.psw;
    t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).repsw = user.repsw;
    console.log(t.find(e => e.id === parseInt(localStorage.getItem('key'), 10)));
    localStorage.setItem('users', JSON.stringify(t));
  }
}
