import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup } from "@angular/forms";
import { DataService } from '../data.service';
import { Router } from "@angular/router";
import {error} from "util";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  flag: boolean;
  invalid: boolean;
  display: boolean;
  loginForm = new FormGroup ({
    name: new FormControl('', [
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  });

  constructor(private dataService: DataService, private router: Router) {
    this.dataService.getUsers()
      .then(users => localStorage.setItem(('users'), JSON.stringify(users)));
    this.dataService.getAllArticles()
      .then(articles => localStorage.setItem(('articles'), JSON.stringify(articles)));
  }

  ngOnInit() {
    this.invalid = false;
    this.display = false;
  }

  login(name: string, psw: string) {
    return this.dataService.getUsers()
      .then(users => {
        if (users.findIndex(user => user.accountName === name) < 0) {
          this.invalid = true;
          this.display = true;
        }
        return users;
      })
      .then(users => {
        this.flag = users.find(e => e.accountName === name).psw === psw;
        return users;
      })
      .then(res => {
        // console.log(this.flag);
        if (this.flag) {
          localStorage.setItem('key', res.find(e => e.accountName === name).id.toString());
        } else {
          this.invalid = true;
          this.display = true;
          return this.invalid;
        }
      })
      .then(_ => {
        if (!this.invalid) this.router.navigate(['./main']);
        else this.invalid = false;
      });
  }
}
