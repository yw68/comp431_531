import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const articles = JSON.parse(localStorage.getItem('articles')) || [
      {
        id: 5996814,
        text: "Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius.",
        date: "2015-05-07T06:48:07.159Z",
        img: null,
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw68",
        display: true
      },
      {
        id: 3677827,
        text: "Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio.\r",
        date: "2015-09-03T17:46:28.488Z",
        img: null,
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw68",
        display: true
      },
      {
        id: 3677777,
        text: "Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.\r",
        date: "2015-05-28T08:35:07.456Z",
        img: null,
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw68",
        display: true
      },
      {
        id: 5025279,
        text: "Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti.\r",
        date: "2015-05-10T09:48:10.804Z",
        img: "http://lorempixel.com/353/202/",
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw69",
        display: true
      },
      {
        id: 4216698,
        text: "Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus.\r",
        date: "2015-05-03T16:51:22.735Z",
        img: "http://lorempixel.com/301/219/",
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw69",
        display: true
      },
      {
        id: 4858339,
        text: "Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit.\r",
        date: "2015-08-01T11:03:13.809Z",
        img: "http://lorempixel.com/351/264/",
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw70",
        display: true
      },
      {
        id: 3484067,
        text: "Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus.\r",
        date: "2015-06-14T08:51:26.380Z",
        img: null,
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw70",
        display: true
      },
      {
        id: 3468980,
        text: "Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh.\r",
        date: "2015-07-27T13:02:22.152Z",
        img: "http://lorempixel.com/319/220/",
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw70",
        display: true
      },
      {
        id: 5349642,
        text: "In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem.\r",
        date: "2015-07-31T18:48:44.631Z",
        img: "http://lorempixel.com/383/294/",
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw69",
        display: true
      },
      {
        id: 4545989,
        text: "In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna.\r",
        date: "2015-06-01T08:49:10.442Z",
        img: null,
        comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
        author: "yw68",
        display: true
      }
    ];
    const users = JSON.parse(localStorage.getItem('users')) || [
      {
        id: 2,
        accountName: "yw68",
        displayName: "SpiderMan",
        phoneNumber: '111-111-1111',
        zip: '11111',
        email: 'yw68@rice.edu',
        psw: '111-111-111',
        repsw: '111-111-111',
        dateOfBirth: '01/01/1990',
        status: "I love Lily",
        img: "http://24.media.tumblr.com/9b0feea4ffd2f30aa239b0aa3a768136/tumblr_mtg05mfMgL1rhlpqgo1_250.gif",
        following: [1],
        articles: [5296814, 3677827, 3677777],
        display: true
      },
      {
        id: 1,
        accountName: "yw69",
        displayName: "SuperMan",
        phoneNumber: '222-222-2222',
        zip: '22222',
        email: 'yw69@rice.edu',
        psw: '222-222-222',
        repsw: '222-222-222',
        dateOfBirth: '02/02/1990',
        status: "I love Jack",
        img: "http://25.media.tumblr.com/d5a9cae07d1346e59ece1df7ceb66767/tumblr_mkc6arj4eE1s5ovhio1_250.gif",
        following: [0, 2],
        articles: [5025279, 4216698, 4858339],
        display: true
      },
      {
        id: 0,
        accountName: "yw70",
        displayName: "SillyBoy",
        phoneNumber: '333-333-3333',
        zip: '33333',
        email: 'yw70@rice.edu',
        psw: '333-333-333',
        repsw: '333-333-333',
        dateOfBirth: '03/03/1990',
        status: "I love panda",
        img: "http://24.media.tumblr.com/tumblr_mccttfRTgk1qeo4gjo1_500.jpg",
        following: [1, 2],
        articles: [3484067, 3468980, 5349642, 4545989],
        display: true
      }
    ];
    const comments = JSON.parse(localStorage.getItem('comments')) || [];
    const data = {
      articles,
      users,
      comments
    };
    return {data};
  }
}
