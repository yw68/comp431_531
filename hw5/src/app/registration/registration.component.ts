import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { DataService } from "../data.service";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  model = new User(0, '', '', '', '', '', '', '', '', '', [], [],  true, '');
  submitted = false;
  id = 0;
  constructor(private dataService: DataService) {

  }
  ngOnInit() {
    this.submitted = true;
  }
  register() {
    this.dataService.getUsers()
      .then(users => {
        this.id = users[0].id + 1;
        return users;
      })
      .then(users => {
        this.model = new User(this.id, '', '', '', '', '', '', '', '', '', [], [],  true, '');
        return users;
      })
      .then(users => users.unshift(this.model));
  }
}
