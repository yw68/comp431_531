export class Follower {
  name: string;
  status: string;
  img: string;
  display: boolean;

  constructor(name: string,
              status: string,
              img: string,
              display: boolean) {
    this.name = name;
    this.status = status;
    this.img = img;
    this.display = display;
  }
}
