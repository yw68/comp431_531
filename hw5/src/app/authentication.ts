import { url, resource } from './resource';
export const login = (name, psw) => {
  return resource('PUT', 'login', {});
};
export const logout = () => {
  return resource('PUT', 'logout', {});
};
