import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { DataService } from "../data.service";
import {current} from "codelyzer/util/syntaxKind";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  model: User;
  current: User;
  constructor(private dataService: DataService) {
  }
  onSubmit(): void {
    console.log(this.current.psw);
    this.dataService.saveInfo(this.current);
    this.model.displayName = this.current.displayName;
    this.model.phoneNumber = this.current.phoneNumber;
    this.model.zip = this.current.zip;
    this.model.email = this.current.email;
    this.model.psw = this.current.psw;
    this.model.repsw = this.current.repsw;
    this.current = new User(0, '', '', '', '', '', '', '', '', '', [], [],  true, '');
  }
  ngOnInit() {
    this.dataService.getUsers().then(users => this.model = users.find(e => e.id === parseInt(localStorage.getItem('key'), 10)));
    this.current = new User(0, '', '', '', '', '', '', '', '', '', [], [],  true, '');
    this.model = new User(0, '', '', '', '', '', '', '', '', '', [], [],  true, '');
  }

}
