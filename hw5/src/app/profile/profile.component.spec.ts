import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { url, resource } from '../resource';
import { fetchInfo, update } from '../profileActions';
import {RouterTestingModule} from "@angular/router/testing";
import {ReactiveFormsModule} from "@angular/forms";
import { FormsModule } from '@angular/forms';
import {HttpModule} from "@angular/http";

import 'fetch-mock';
import {DataService} from "../data.service";

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileComponent ],
      imports: [RouterTestingModule, ReactiveFormsModule, FormsModule, HttpModule],
      providers: [DataService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  const fetchMock = require('fetch-mock');
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should fetch the user\'s profile information', (done) => {
    const user = { name: 'yw', email: 'yw@exam.com', phoneNumber: '111-111-1234' };
    fetchMock.mock(`${url}/profile`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'},
      json: { name: 'yw', email: 'yw@exam.com', phoneNumber: '111-111-1234' }
    });
    fetchInfo()
      .then(res => expect(res).toEqual(user))
      .then(done)
      .catch(done);
  });
  it('should update headline', (done) => {
    fetchMock.mock(`${url}/profile`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      json: { status: 'Success' }
    });
    update('new headline')
      .then(res => expect(res.status).toEqual('Success'))
      .then(done)
      .catch(done);
  });
});
