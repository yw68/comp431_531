import { Component, OnInit } from '@angular/core';


import { Article } from "../article";
import { User } from "../user";
import { DataService } from '../data.service';
import { Router } from "@angular/router";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  user: User;
  articles: Article[];
  followers: User[];
  followersID: number[];

  constructor(private dataService: DataService, private router: Router) {
    this.dataService.getUsers()
      .then(users => this.user = users.find(e => e.id === parseInt(localStorage.getItem('key'), 10)));
    this.user = new User(0, '', '', '', '', '', '', '', '', '', [], [], true);
  }

  getArticles() {
    return this.dataService
      .getArticles(parseInt(localStorage.getItem('key'), 10))
      .then(articles => this.articles = articles);
  }
  getAllArticles() {
    return this.dataService
      .getAllArticles()
      .then(articles => this.articles = articles);
  }
  getFollowers(): void {
    this.dataService
      .getUsers()
      .then(res => {
        this.followersID = res.find(e => e.id === parseInt(localStorage.getItem('key'), 10)).following;
        return res;
      })
      .then(users => this.followers = users.filter(e => this.followersID.includes(e.id)))
      .then(() => console.log(this.followers));
  }
  filterArticles(inputKey: string): void {
    if (inputKey != null) this.articles.forEach(function(element) {
      if (element.author.indexOf(inputKey) === -1 && element.text.indexOf(inputKey) === -1) element.display = false;
    });
    if (inputKey === '') this.articles.forEach(x => x.display = true);
  }
  deleteFollower(name: String): void {
    this.dataService
      .getUsers()
      .then(users => users.find(e => e.accountName === name).id)
      .then(followerID => this.dataService.deleteFollower(parseInt(localStorage.getItem('key'), 10), followerID));
    const t = this.dataService.refreshFollower1(this.articles, this.followers, name);
    this.articles = t.articles;
    this.followers = t.follow;
  }
  addFriend(inputAccount: string): void {
    this.dataService.getUsers()
      .then(users => users.findIndex(e => e.accountName === inputAccount))
      .then(res => {
        if (res < 0) return false;
        else {
          this.dataService.addFollower(parseInt(localStorage.getItem('key'), 10), res);
          return true;
        }
      })
      .then(res => {
        if (res) {
          const t = this.dataService.refreshFollower(this.articles, this.followers, inputAccount);
          this.articles = t.articles;
          this.followers = t.follow;
        }
      });
  }
  post(inputText: string): void {
    this.dataService
      .addPost(parseInt(localStorage.getItem('key'), 10), inputText)
      .then(_ => {
        this.articles = JSON.parse(localStorage.getItem('articles'));
      })
      .then(_ => console.log(this.articles));
  }
  updateStatus(inputStatus: string): void {
    this.user.status = inputStatus;
    this.dataService.setStatus(inputStatus);
  }
  hideComments(articleId) {
    const comments = document.getElementById(articleId + 'comments');
    const button = document.getElementById ('hideComments');
    if (comments.style.display === 'none') {
      button.innerHTML = 'Hide Comments';
      comments.style.display = 'block';
    } else {
      button.innerHTML = 'Show Comments';
      comments.style.display = 'none';
    }
  }
  ngOnInit() {
    this.getArticles();
    this.getFollowers();
  }
}
