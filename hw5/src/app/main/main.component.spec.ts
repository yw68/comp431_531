import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import { MainComponent } from './main.component';
import {DataService} from "../data.service";
import { By } from '@angular/platform-browser';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainComponent ],
      imports: [HttpModule, FormsModule, ReactiveFormsModule, RouterTestingModule],
      providers: [DataService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render articles', () => {
    component.getAllArticles();
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('#articles'));
    expect(0).toEqual(element.children.length);
  });
});
