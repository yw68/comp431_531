import { url, resource } from './resource';
export const beResource = () => {
  return resource('GET', 'resource', {});
};
export const getError = (post) => {
  return resource('POST', 'getPosts', {
    post
  });
};
export const postable = (post) => {
  return resource('PUT', 'postable', {post});
};
export const navigate = (dir) => {
  if (dir === 'main') return resource('POST', 'naviMain', {dir});
  if (dir === 'profile') return resource('POST', 'naviProfile', {dir});
  if (dir === 'registration') return resource('POST', 'naviRegistration', {dir});
};
