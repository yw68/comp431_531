import {TestBed, inject} from '@angular/core/testing';
import {url, resource} from './resource';
import {DataService} from './data.service';
import {getError, beResource, postable, navigate} from "./validateActions";
import {fetchArticles, filterArticles, searchKey} from './articleActions';
import 'fetch-mock';
import {HttpModule} from "@angular/http";

const fetchMock = require('fetch-mock');
describe('Validate Article actions', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService],
      imports: [HttpModule]
    });
  });
  it('should be created', inject([DataService], (service: DataService) => {
    expect(service).toBeTruthy();
  }));
  it('should fetch articles', (done) => {
    const articles = [{author: 'yw', content: 'This is an article'}, {
      author: 'lw',
      content: 'This is an another article'
    }];
    fetchMock.mock(`${url}/articles`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'},
      json: {articles}
    });
    fetchArticles()
      .then(res => expect(res).toEqual(articles))
      .then(done)
      .catch(done);
  });
  it('should update the search keyword', (done) => {
    const newSearchKey = 'newSeachKey';
    let a = searchKey(newSearchKey);
    expect(a).toEqual(newSearchKey);
    done();
  });
  it('should filter displayed articles by the search keyword', () => {
    const searchArticle = 'yw';
    const articles = [{author: 'yw', content: 'This is an article'}, {
      author: 'lw',
      content: 'This is an another article'
    }];
    const res = {author: 'yw', content: 'This is an article'};
    expect(filterArticles(searchArticle, articles)[0]).toEqual(res);

  });
});
describe('Validate actions', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService],
      imports: [HttpModule]
    });
  });
  it('resource should be a resource', (done) => {
    fetchMock.mock(`${url}/resource`, {
      method: 'GET',
      headers: {'Content-Type': 'application/json'},
      json: {status: 'Success'}
    });
    beResource()
      .then(res => expect(res.status).toEqual('Success'))
      .then(done)
      .catch(done);
  });
  it('resource should give me the http error', (done) => {

    fetchMock.mock(`${url}/getSomething`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: {status: 'fail'}
    }).spy();

    getError('test').then(r => {
      expect(r.status).toEqual('Success');
    }).then(done)
      .catch(r => {
        expect(r.toString().indexOf('Error')).not.toEqual(-1);
      });
    done();
  });
  it('resource should be POSTable', () => {
    fetchMock.mock(`${url}/postable`, {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: {status: 'Success'}
    });
    postable('test').then(r => expect(r.status).toEqual('Success'));
  });
  it('should navigate (to profile, main, or landing)', (done) => {

    fetchMock.mock(`${url}/naviMain`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: {result: `${url}/main`}
    }).spy();

    fetchMock.mock(`${url}/naviProfile`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: {result: `${url}/profile`}
    }).spy();

    fetchMock.mock(`${url}/naviRegistration`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: {result: `${url}/registration`}
    }).spy();

    navigate('main').then(r => {
      expect(r.result).toEqual(`${url}/main`);
    }).then(done)
      .catch(done);

    navigate('profile').then(r => {
      expect(r.result).toEqual(`${url}/profile`);
    }).then(done)
      .catch(done);

    navigate('registration').then(r => {
      expect(r.result).toEqual(`${url}/registration`);
    }).then(done)
      .catch(done);
  });
});


