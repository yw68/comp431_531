webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__landing_landing_component__ = __webpack_require__("../../../../../src/app/landing/landing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__profile_profile_component__ = __webpack_require__("../../../../../src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__registration_registration_component__ = __webpack_require__("../../../../../src/app/registration/registration.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    { path: '', redirectTo: '/landing', pathMatch: 'full' },
    { path: 'landing', component: __WEBPACK_IMPORTED_MODULE_3__landing_landing_component__["a" /* LandingComponent */] },
    { path: 'main', component: __WEBPACK_IMPORTED_MODULE_4__main_main_component__["a" /* MainComponent */] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_5__profile_profile_component__["a" /* ProfileComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_6__login_login_component__["a" /* LoginComponent */] },
    { path: 'registration', component: __WEBPACK_IMPORTED_MODULE_7__registration_registration_component__["a" /* RegistrationComponent */] }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(routes, { useHash: true })
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */]],
        declarations: []
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>{{title}}</h1>\n\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'hw5';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile_component__ = __webpack_require__("../../../../../src/app/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular_in_memory_web_api_in_memory_web_api_module__ = __webpack_require__("../../../../angular-in-memory-web-api/in-memory-web-api.module.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__in_memory_data_service__ = __webpack_require__("../../../../../src/app/in-memory-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__landing_landing_component__ = __webpack_require__("../../../../../src/app/landing/landing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__main_main_component__ = __webpack_require__("../../../../../src/app/main/main.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing_app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__registration_registration_component__ = __webpack_require__("../../../../../src/app/registration/registration.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__validator_equal_validator_directive__ = __webpack_require__("../../../../../src/app/validator/equal-validator.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__validator_age_validator_directive__ = __webpack_require__("../../../../../src/app/validator/age-validator.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_8__landing_landing_component__["a" /* LandingComponent */],
            __WEBPACK_IMPORTED_MODULE_9__main_main_component__["a" /* MainComponent */],
            __WEBPACK_IMPORTED_MODULE_4__profile_profile_component__["a" /* ProfileComponent */],
            __WEBPACK_IMPORTED_MODULE_11__registration_registration_component__["a" /* RegistrationComponent */],
            __WEBPACK_IMPORTED_MODULE_12__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_13__validator_equal_validator_directive__["a" /* EqualValidator */],
            __WEBPACK_IMPORTED_MODULE_15__validator_age_validator_directive__["a" /* AgeValidator */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_10__app_routing_app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_5_angular_in_memory_web_api_in_memory_web_api_module__["a" /* InMemoryWebApiModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__in_memory_data_service__["a" /* InMemoryDataService */]),
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["e" /* ReactiveFormsModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_14__data_service__["a" /* DataService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/article.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Article; });
var Article = (function () {
    function Article(id, text, date, img, comments, author, display) {
        this.id = id;
        this.text = text;
        this.date = date;
        this.img = img;
        this.comments = comments;
        this.author = author;
        this.display = display;
    }
    return Article;
}());

//# sourceMappingURL=article.js.map

/***/ }),

/***/ "../../../../../src/app/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__article__ = __webpack_require__("../../../../../src/app/article.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user__ = __webpack_require__("../../../../../src/app/user.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DataService = (function () {
    function DataService(http) {
        this.http = http;
        this.dataUrl = 'api/data';
    }
    DataService.prototype.handleError = function (error) {
        console.error('An HTTP error occurred', error);
        return Promise.reject(error.message || error);
    };
    DataService.prototype.getArticles = function (id) {
        var _this = this;
        // console.log(localStorage.getItem('data'));
        var owners = new Array();
        var t;
        return this.http.get(this.dataUrl)
            .toPromise()
            .then(function (res) {
            console.log(res);
            return res;
        })
            .then(function (res) {
            console.log(id);
            t = res.json();
            return _this.getUsers();
        })
            .then(function (users) {
            owners.push(users.find(function (p) { return p.id === id; }).accountName);
            console.log(id);
            console.log(owners);
            var temp = users.find(function (p) { return p.id === id; }).following;
            console.log(temp);
            var _loop_1 = function (i) {
                owners.push(users.find(function (user) { return user.id === temp[i]; }).accountName);
            };
            for (var i = 0; i < temp.length; i++) {
                _loop_1(i);
            }
            console.log(owners);
        })
            .then(function (_) { return t.articles; })
            .then(function (articles) {
            console.log(owners);
            return articles.filter(function (article) { return owners.includes(article.author); });
        })
            .catch(this.handleError);
    };
    DataService.prototype.getAllArticles = function () {
        return this.http.get(this.dataUrl)
            .toPromise()
            .then(function (response) { return response.json().articles; })
            .catch(this.handleError);
    };
    DataService.prototype.getUsers = function () {
        return this.http.get(this.dataUrl)
            .toPromise()
            .then(function (response) { return response.json().users; })
            .catch(this.handleError);
    };
    DataService.prototype.deleteFollower = function (user, follower) {
        return this.http.get(this.dataUrl)
            .toPromise()
            .then(function (res) { return res.json().users; })
            .then(function (users) {
            users.find(function (e) { return e.id === user; }).following = users.find(function (e) { return e.id === user; }).following.filter(function (e) { return e !== follower; });
            localStorage.setItem('users', JSON.stringify(users));
        })
            .catch(this.handleError);
    };
    DataService.prototype.addFollower = function (user, follower) {
        return this.http.get(this.dataUrl)
            .toPromise()
            .then(function (res) { return res.json().users; })
            .then(function (users) {
            users.find(function (e) { return e.id === user; }).following.push(follower);
            localStorage.setItem('users', JSON.stringify(users));
        })
            .catch(this.handleError);
    };
    DataService.prototype.refreshFollower = function (articles, follow, input) {
        follow.push(JSON.parse(localStorage.getItem('users')).find(function (e) { return e.accountName === input; }));
        this.getAllArticles()
            .then(function (res) { return articles.push(res.find(function (e) { return e.author === input; })); });
        return { follow: follow, articles: articles };
    };
    DataService.prototype.refreshFollower1 = function (articles, follow, input) {
        var x1 = follow.filter(function (e) { return e.accountName !== input; });
        var x2 = articles.filter(function (e) { return e.author !== input; });
        return { follow: x1, articles: x2 };
    };
    DataService.prototype.addUser = function (id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, display, displayName) {
        var u = new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */](id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, display, displayName);
        this.http.get(this.dataUrl)
            .toPromise()
            .then(function (res) { return res.json().users; })
            .then(function (users) { return users.push(u); })
            .then(function (newUsers) { return localStorage.setItem('users', JSON.stringify(newUsers)); })
            .catch(this.handleError);
    };
    // loginUser(userName: string, psw: string): Promise<boolean> {
    //   return this.http.get(this.dataUrl)
    //     .toPromise()
    //     .then(res => res.json().users as User[])
    //     .then(res => res.find(e => e.accountName === userName))
    //     .then(res => res.psw === psw)
    //     .catch(this.handleError);
    // }
    DataService.prototype.addPost = function (id, content) {
        var index = 0;
        var name = '';
        var articles;
        return this.http.get(this.dataUrl)
            .toPromise()
            .then(function (res) {
            name = res.json().users.find(function (e) { return e.id === id; }).accountName;
            index = res.json().articles[0].id + 1;
            articles = res.json().articles;
            articles.unshift(new __WEBPACK_IMPORTED_MODULE_3__article__["a" /* Article */](index, content, new Date().toString(), '', [], name, true));
            localStorage.setItem('articles', JSON.stringify(articles));
            return res;
        })
            .then(function (res) { return res.json().users; })
            .then(function (res) { return res.find(function (e) { return e.id === id; }).articles.push(index); })
            .catch(this.handleError);
    };
    DataService.prototype.editUser = function (phoneNumber, zip, email, psw, repsw, dateOfBirth, displayName) {
        this.http.get(this.dataUrl)
            .toPromise()
            .then(function (res) { return res.json().users; })
            .then(function (usrs) {
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).phoneNumber = phoneNumber;
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).zip = zip;
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).email = email;
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).psw = psw;
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).repsw = repsw;
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).dateOfBirth = dateOfBirth;
            usrs.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).displayName = displayName;
            localStorage.setItem('users', JSON.stringify(usrs));
        });
    };
    DataService.prototype.setStatus = function (input) {
        var t = JSON.parse(localStorage.getItem('users'));
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).status = input;
        localStorage.setItem('users', JSON.stringify(t));
    };
    DataService.prototype.saveInfo = function (user) {
        var t = JSON.parse(localStorage.getItem('users'));
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).displayName = user.displayName;
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).email = user.email;
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).phoneNumber = user.phoneNumber;
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).zip = user.zip;
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).psw = user.psw;
        t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).repsw = user.repsw;
        console.log(t.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }));
        localStorage.setItem('users', JSON.stringify(t));
    };
    return DataService;
}());
DataService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], DataService);

var _a;
//# sourceMappingURL=data.service.js.map

/***/ }),

/***/ "../../../../../src/app/in-memory-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InMemoryDataService; });
var InMemoryDataService = (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        var articles = JSON.parse(localStorage.getItem('articles')) || [
            {
                id: 5996814,
                text: "Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius.",
                date: "2015-05-07T06:48:07.159Z",
                img: null,
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw68",
                display: true
            },
            {
                id: 3677827,
                text: "Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio.\r",
                date: "2015-09-03T17:46:28.488Z",
                img: null,
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw68",
                display: true
            },
            {
                id: 3677777,
                text: "Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.\r",
                date: "2015-05-28T08:35:07.456Z",
                img: null,
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw68",
                display: true
            },
            {
                id: 5025279,
                text: "Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti.\r",
                date: "2015-05-10T09:48:10.804Z",
                img: "http://lorempixel.com/353/202/",
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw69",
                display: true
            },
            {
                id: 4216698,
                text: "Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus.\r",
                date: "2015-05-03T16:51:22.735Z",
                img: "http://lorempixel.com/301/219/",
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw69",
                display: true
            },
            {
                id: 4858339,
                text: "Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit.\r",
                date: "2015-08-01T11:03:13.809Z",
                img: "http://lorempixel.com/351/264/",
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw70",
                display: true
            },
            {
                id: 3484067,
                text: "Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus.\r",
                date: "2015-06-14T08:51:26.380Z",
                img: null,
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw70",
                display: true
            },
            {
                id: 3468980,
                text: "Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh.\r",
                date: "2015-07-27T13:02:22.152Z",
                img: "http://lorempixel.com/319/220/",
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw70",
                display: true
            },
            {
                id: 5349642,
                text: "In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem.\r",
                date: "2015-07-31T18:48:44.631Z",
                img: "http://lorempixel.com/383/294/",
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw69",
                display: true
            },
            {
                id: 4545989,
                text: "In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna.\r",
                date: "2015-06-01T08:49:10.442Z",
                img: null,
                comments: ["yw68 : I am hadnsome", "yw68 : Yes, you are totally right"],
                author: "yw68",
                display: true
            }
        ];
        var users = JSON.parse(localStorage.getItem('users')) || [
            {
                id: 2,
                accountName: "yw68",
                displayName: "SpiderMan",
                phoneNumber: '111-111-1111',
                zip: '11111',
                email: 'yw68@rice.edu',
                psw: '111-111-111',
                repsw: '111-111-111',
                dateOfBirth: '01/01/1990',
                status: "I love Lily",
                img: "http://24.media.tumblr.com/9b0feea4ffd2f30aa239b0aa3a768136/tumblr_mtg05mfMgL1rhlpqgo1_250.gif",
                following: [1],
                articles: [5296814, 3677827, 3677777],
                display: true
            },
            {
                id: 1,
                accountName: "yw69",
                displayName: "SuperMan",
                phoneNumber: '222-222-2222',
                zip: '22222',
                email: 'yw69@rice.edu',
                psw: '222-222-222',
                repsw: '222-222-222',
                dateOfBirth: '02/02/1990',
                status: "I love Jack",
                img: "http://25.media.tumblr.com/d5a9cae07d1346e59ece1df7ceb66767/tumblr_mkc6arj4eE1s5ovhio1_250.gif",
                following: [0, 2],
                articles: [5025279, 4216698, 4858339],
                display: true
            },
            {
                id: 0,
                accountName: "yw70",
                displayName: "SillyBoy",
                phoneNumber: '333-333-3333',
                zip: '33333',
                email: 'yw70@rice.edu',
                psw: '333-333-333',
                repsw: '333-333-333',
                dateOfBirth: '03/03/1990',
                status: "I love panda",
                img: "http://24.media.tumblr.com/tumblr_mccttfRTgk1qeo4gjo1_500.jpg",
                following: [1, 2],
                articles: [3484067, 3468980, 5349642, 4545989],
                display: true
            }
        ];
        var comments = JSON.parse(localStorage.getItem('comments')) || [];
        var data = {
            articles: articles,
            users: users,
            comments: comments
        };
        return { data: data };
    };
    return InMemoryDataService;
}());

//# sourceMappingURL=in-memory-data.service.js.map

/***/ }),

/***/ "../../../../../src/app/landing/landing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/landing/landing.component.html":
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n<div class=\"container\" style=\"width:25%\">\n  <nav>\n    <a routerLink=\"/login\" routerLinkActive=\"active\">Log in</a>\n    <a routerLink=\"/registration\" routerLinkActive=\"active\">Sign up</a>\n  </nav>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/landing/landing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LandingComponent = (function () {
    function LandingComponent() {
    }
    LandingComponent.prototype.ngOnInit = function () {
    };
    return LandingComponent;
}());
LandingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-landing',
        template: __webpack_require__("../../../../../src/app/landing/landing.component.html"),
        styles: [__webpack_require__("../../../../../src/app/landing/landing.component.css")]
    }),
    __metadata("design:paramtypes", [])
], LandingComponent);

//# sourceMappingURL=landing.component.js.map

/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"width:25%\">\n  <h2>Log in</h2>\n\n  <form [formGroup]=\"loginForm\" novalidate>\n    <div class=\"form-group\">\n      <input type=\"text\" class=\"form-control\"\n             #name\n             id=\"account\"\n             required\n             placeholder=\"Account Name\"\n             formControlName=\"name\">\n    </div>\n    <div class=\"form-group\">\n      <input type=\"password\" class=\"form-control\"\n             #psw\n             required\n             placeholder=\"Password\"\n             formControlName=\"password\">\n    </div>\n    <span *ngIf=\"display\">Wrong name or password</span><br><br>\n    <button [disabled]=\"!loginForm.valid\"\n            class=\"btn btn-default\"\n            (click) = \"login(name.value, psw.value)\">\n      Log in\n    </button>\n  </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = (function () {
    function LoginComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.loginForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required
            ]),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', [
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required
            ])
        });
        this.dataService.getUsers()
            .then(function (users) { return localStorage.setItem(('users'), JSON.stringify(users)); });
        this.dataService.getAllArticles()
            .then(function (articles) { return localStorage.setItem(('articles'), JSON.stringify(articles)); });
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.invalid = false;
        this.display = false;
    };
    LoginComponent.prototype.login = function (name, psw) {
        var _this = this;
        return this.dataService.getUsers()
            .then(function (users) {
            if (users.findIndex(function (user) { return user.accountName === name; }) < 0) {
                _this.invalid = true;
                _this.display = true;
            }
            return users;
        })
            .then(function (users) {
            _this.flag = users.find(function (e) { return e.accountName === name; }).psw === psw;
            return users;
        })
            .then(function (res) {
            // console.log(this.flag);
            if (_this.flag) {
                localStorage.setItem('key', res.find(function (e) { return e.accountName === name; }).id.toString());
            }
            else {
                _this.invalid = true;
                _this.display = true;
                return _this.invalid;
            }
        })
            .then(function (_) {
            if (!_this.invalid)
                _this.router.navigate(['./main']);
            else
                _this.invalid = false;
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/main/main.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card {\n  margin: 0 auto;\n  width: 75%;\n  margin-bottom: 1.5pt;\n  padding: 3pt;\n  border: 1px solid #eeeeee;\n  border-radius: 10px;\n  min-height: 3.5em;\n  background-color:rgba(255,255,255,0.95);\n}\n.wrap {\n  min-width: 100%;\n  position: absolute;\n  background: #eff3f6 bottom;\n  min-height: 100%;\n  overflow: hidden;\n}\n.left{\n  margin: 0 auto;\n  margin-bottom: 1.5pt;\n  padding: 3pt;\n  border: 1px solid #eeeeee;\n  border-radius: 10px;\n  min-height: 3.5em;\n  background-color: white;\n  margin-left:10px;\n  position: absolute;\n  box-sizing: border-box;\n  width: 210px;\n  height: auto;\n}\n.logoDiv{\n  padding-top: 20px;\n  padding-bottom: 20px;\n  height: 70px;\n  background-color: #354457;\n  font-size: 17px;\n  color: #fff;\n  vertical-align: bottom;\n}\n.logoTitle{\n  margin-left:15px;\n  line-height: 1.7;\n}\n.menu-title {\n  margin-left:15px;\n  color: #828e9a;\n  padding-top: 10px;\n  padding-bottom: 10px;\n  font-size: 14px;\n  font-weight: bold;\n}\n.menu-item {\n  padding-left:15px;\n  line-height: 40px;\n  height: 40px;\n  color: #aab1b7;\n  cursor: pointer;\n}\n.menu-item-active {\n  background-color: #3d4e60;\n  border-right: 4px solid #647f9d;\n  color: #fff;\n}\n.right{\n  box-sizing: border-box;\n  float: left;\n  box-sizing: border-box;\n  margin-left: 120px;\n  overflow-y: overlay;\n  overflow-x: hidden;\n  clear: both;\n  color: #717592;\n  min-width: 750px;\n  min-height: 500px;\n}\n.back{\n  background: url(https://upload.wikimedia.org/wikipedia/commons/d/db/Cijin_District_view_from_Mt_QiHou.jpg);\n  background-attachment:fixed;\n}\n\n\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/main.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"back\" style=\"padding-bottom: 50px\">\n  <div class=\"container\">\n    <div class=\"left\" style=\"margin-top: 20px; background-color:rgba(255,255,255,0.95)\">\n      <h1>Rice Book</h1>\n\n      <nav>\n        <a routerLink=\"/landing\" routerLinkActive=\"active\">Log out</a>\n        <a routerLink=\"/profile\" routerLinkActive=\"active\">Profile</a>\n      </nav>\n      <br>\n      <img src={{user.img}} height=\"192px\"/>\n      <br>\n      <p>Name: {{user.accountName}}</p>\n      <p>Status: {{user.status}}</p>\n\n      <input #status class=\"form-control\"/>\n      <button (click)=\"updateStatus(status.value); status.value=''\" class=\"btn btn-default\">\n        Update status\n      </button>\n\n      <h1>Friends</h1>\n      <div *ngFor=\"let follower of followers\">\n        <div *ngIf=\"follower.display\">\n          <br>\n          <img src={{follower.img}} width=\"200px\">\n          <p>Name: {{follower.accountName}}</p>\n          <span>Status: {{follower.status}}</span>\n          <br>\n          <button type=\"button\" class=\"btn btn-default\">Profile</button>\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"deleteFollower(follower.accountName)\">Delete</button>\n        </div>\n      </div>\n      <div style=\"padding-top: 10px\">\n        <input #add class=\"form-control\"/>\n        <button (click)=\"addFriend(add.value); add.value=''\" class=\"btn btn-default\">\n          Add friend\n        </button>\n      </div>\n\n    </div>\n    <div class=\"container\" style=\"margin-top: 20px; min-height: 2000px\">\n      <div class=\"right\">\n\n        <div class=\"card\">\n          <textarea class=\"form-control\" rows=\"5\" #textarea placeholder=\"Your post here\"></textarea>\n          <button (click)=\"textarea.value=''\" type=\"button\" class=\"btn btn-default\">cancel</button>\n          <button (click)=\"(post(textarea.value)); textarea.value=''\" type=\"button\" class=\"btn btn-default\">post\n          </button>\n          <input type=\"file\">\n        </div>\n\n        <div class=\"card\" style=\"padding-top: 20px\">\n          <input #key class=\"form-control\"/>\n          <button (click)=\"(filterArticles(key.value))\" class=\"btn btn-default\" style=\"align:right;\">\n            Search\n          </button>\n        </div>\n\n        <div id=\"articles\">\n          <ng-container *ngFor=\"let article of articles\" class=\"card\">\n            <div class=\"card\" *ngIf=\"article.display\">\n              <span>On {{article.date}}, {{article.author}} said:</span>\n              <br>\n              <img src={{article.img}}>\n              <br>\n              {{article.text}}\n              <br><br>\n              <button type=\"button\" class=\"btn btn-default\">edit</button>\n              <button type=\"button\" class=\"btn btn-default\">comment</button>\n              <button class=\"button3\" class=\"btn btn-default\" id=\"hideComments\" type=\"button\" (click)=\"hideComments(article.id)\">Hide Comments\n              </button>\n              <div id=\"{{article.id}}comments\">\n                <div *ngFor=\"let com of article.comments\">\n                  <p class=\"comments\">{{com}}</p>\n                </div>\n              </div>\n            </div>\n          </ng-container>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <router-outlet></router-outlet>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/main/main.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MainComponent = (function () {
    function MainComponent(dataService, router) {
        var _this = this;
        this.dataService = dataService;
        this.router = router;
        this.dataService.getUsers()
            .then(function (users) { return _this.user = users.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }); });
        this.user = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', '', '', '', [], [], true);
    }
    MainComponent.prototype.getArticles = function () {
        var _this = this;
        return this.dataService
            .getArticles(parseInt(localStorage.getItem('key'), 10))
            .then(function (articles) { return _this.articles = articles; });
    };
    MainComponent.prototype.getAllArticles = function () {
        var _this = this;
        return this.dataService
            .getAllArticles()
            .then(function (articles) { return _this.articles = articles; });
    };
    MainComponent.prototype.getFollowers = function () {
        var _this = this;
        this.dataService
            .getUsers()
            .then(function (res) {
            _this.followersID = res.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }).following;
            return res;
        })
            .then(function (users) { return _this.followers = users.filter(function (e) { return _this.followersID.includes(e.id); }); })
            .then(function () { return console.log(_this.followers); });
    };
    MainComponent.prototype.filterArticles = function (inputKey) {
        if (inputKey != null)
            this.articles.forEach(function (element) {
                if (element.author.indexOf(inputKey) === -1 && element.text.indexOf(inputKey) === -1)
                    element.display = false;
            });
        if (inputKey === '')
            this.articles.forEach(function (x) { return x.display = true; });
    };
    MainComponent.prototype.deleteFollower = function (name) {
        var _this = this;
        this.dataService
            .getUsers()
            .then(function (users) { return users.find(function (e) { return e.accountName === name; }).id; })
            .then(function (followerID) { return _this.dataService.deleteFollower(parseInt(localStorage.getItem('key'), 10), followerID); });
        var t = this.dataService.refreshFollower1(this.articles, this.followers, name);
        this.articles = t.articles;
        this.followers = t.follow;
    };
    MainComponent.prototype.addFriend = function (inputAccount) {
        var _this = this;
        this.dataService.getUsers()
            .then(function (users) { return users.findIndex(function (e) { return e.accountName === inputAccount; }); })
            .then(function (res) {
            if (res < 0)
                return false;
            else {
                _this.dataService.addFollower(parseInt(localStorage.getItem('key'), 10), res);
                return true;
            }
        })
            .then(function (res) {
            if (res) {
                var t = _this.dataService.refreshFollower(_this.articles, _this.followers, inputAccount);
                _this.articles = t.articles;
                _this.followers = t.follow;
            }
        });
    };
    MainComponent.prototype.post = function (inputText) {
        var _this = this;
        this.dataService
            .addPost(parseInt(localStorage.getItem('key'), 10), inputText)
            .then(function (_) {
            _this.articles = JSON.parse(localStorage.getItem('articles'));
        })
            .then(function (_) { return console.log(_this.articles); });
    };
    MainComponent.prototype.updateStatus = function (inputStatus) {
        this.user.status = inputStatus;
        this.dataService.setStatus(inputStatus);
    };
    MainComponent.prototype.hideComments = function (articleId) {
        var comments = document.getElementById(articleId + 'comments');
        var button = document.getElementById('hideComments');
        if (comments.style.display === 'none') {
            button.innerHTML = 'Hide Comments';
            comments.style.display = 'block';
        }
        else {
            button.innerHTML = 'Show Comments';
            comments.style.display = 'none';
        }
    };
    MainComponent.prototype.ngOnInit = function () {
        this.getArticles();
        this.getFollowers();
    };
    return MainComponent;
}());
MainComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-main',
        template: __webpack_require__("../../../../../src/app/main/main.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main/main.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _b || Object])
], MainComponent);

var _a, _b;
//# sourceMappingURL=main.component.js.map

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".left{\n  margin-left:10px;\n  position: absolute;\n  box-sizing: border-box;\n  width: 400px;\n  height: 100%;\n}\n.right{\n  box-sizing: border-box;\n  float: left;\n  padding-left: 400px;\n  overflow-y: overlay;\n  overflow-x: hidden;\n  clear: both;\n  min-width: 100%;\n  min-height: 500px;\n}\n.ng-valid[required], .ng-valid.required  {\n  border-left: 5px solid #42A948; /* green */\n}\n\n.ng-invalid:not(form)  {\n  border-left: 5px solid #a94442; /* red */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"left\">\n    <nav>\n      <a routerLink=\"/main\" routerLinkActive=\"active\">Main page</a>\n    </nav>\n    <router-outlet></router-outlet>\n    <div style=\"padding-top: 20px; width:210px; padding-left: 5px; border:1px solid #eeeeee; border-radius:10px;\">\n      <h2>Current Info</h2>\n      <img src=\"http://24.media.tumblr.com/tumblr_m6r07hduts1r6u1aso1_500.jpg\" height=\"200px\"/>\n      <input type=\"file\">\n      <div class=\"form-group\">\n        <label>Display Name:</label>\n        <p>{{model.displayName}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Email Address:</label>\n        <p>{{model.email}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Phone Number:</label>\n        <p>{{model.phoneNumber}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Date of Birth:</label>\n        <p>{{model.dateOfBirth}}</p>\n      </div>\n\n      <div class=\"form-group\">\n        <label>Zip Code:</label>\n        <p>{{model.zip}}</p>\n      </div>\n    </div>\n  </div>\n  <div class=\"right\">\n    <br><br>\n    <div\n      style=\"padding-top: 20px; width:500px; padding-left: 20px; padding-bottom: 20px; padding-right: 20px; border:1px solid #eeeeee; border-radius:10px;\">\n      <h2>Update Info</h2>\n      <form #userForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\n\n        <div class=\"form-group\">\n          <label for=\"display\">Display Name</label>\n          <input type=\"text\" class=\"form-control\" id=\"display\"\n                 [(ngModel)]=\"current.displayName\" name=\"display\">\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"email\">Email</label>\n          <input type=\"text\" class=\"form-control\" id=\"email\" required\n                 [(ngModel)]=\"current.email\" name=\"email\"\n                 pattern=\"^[a-zA-Z0–9_.+-]+@[a-zA-Z0–9-]+.[a-zA-Z0–9.-]+$\"\n                 #email=\"ngModel\">\n          <div [hidden]=\"email.valid || email.pristine\"\n               class=\"alert alert-danger\">\n            Email is required and format should be <i>john@domain.com</i>.\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"phone\">Phone</label>\n          <input type=\"text\" class=\"form-control\" id=\"phone\" required\n                 [(ngModel)]=\"current.phoneNumber\" name=\"phone\"\n                 pattern=\"^\\d\\d\\d-\\d\\d\\d-\\d\\d\\d\\d$\"\n                 #phone=\"ngModel\">\n          <div [hidden]=\"phone.valid || phone.pristine\"\n               class=\"alert alert-danger\">\n            Phone Number is required and format should be <i>123-456-7890</i>.\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label for=\"zip\">Zip Code</label>\n          <input type=\"number\" class=\"form-control\" id=\"zip\" required\n                 [(ngModel)]=\"current.zip\" name=\"zip\"\n                 pattern=\"^\\d\\d\\d\\d\\d$\"\n                 #zip=\"ngModel\">\n          <div [hidden]=\"zip.valid || zip.pristine\"\n               class=\"alert alert-danger\">\n            Zip Code is required and consists of five integers\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <label>Password</label>\n          <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"current.psw\"\n                 required #password=\"ngModel\" validateEqual=\"confirmPassword\" reverse=\"true\">\n          <div [hidden]=\"password.valid || (password.pristine)\">\n            Password is required\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label>Retype password</label>\n          <input type=\"password\" class=\"form-control\" name=\"confirmPassword\" [(ngModel)]=\"current.repsw\"\n                 required validateEqual=\"password\" #confirmPassword=\"ngModel\">\n          <div [hidden]=\"confirmPassword.valid ||  (confirmPassword.pristine)\">\n            Password mismatch\n          </div>\n        </div>\n        <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!userForm.form.valid\">\n          Submit\n        </button>\n      </form>\n    </div>\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileComponent = (function () {
    function ProfileComponent(dataService) {
        this.dataService = dataService;
    }
    ProfileComponent.prototype.onSubmit = function () {
        console.log(this.current.psw);
        this.dataService.saveInfo(this.current);
        this.model.displayName = this.current.displayName;
        this.model.phoneNumber = this.current.phoneNumber;
        this.model.zip = this.current.zip;
        this.model.email = this.current.email;
        this.model.psw = this.current.psw;
        this.model.repsw = this.current.repsw;
        this.current = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', '', '', '', [], [], true, '');
    };
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getUsers().then(function (users) { return _this.model = users.find(function (e) { return e.id === parseInt(localStorage.getItem('key'), 10); }); });
        this.current = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', '', '', '', [], [], true, '');
        this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', '', '', '', [], [], true, '');
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-profile',
        template: __webpack_require__("../../../../../src/app/profile/profile.component.html"),
        styles: [__webpack_require__("../../../../../src/app/profile/profile.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object])
], ProfileComponent);

var _a;
//# sourceMappingURL=profile.component.js.map

/***/ }),

/***/ "../../../../../src/app/registration/registration.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ng-valid[required], .ng-valid.required  {\n  border-left: 5px solid #42A948; /* green */\n}\n\n.ng-invalid:not(form)  {\n  border-left: 5px solid #a94442; /* red */\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/registration/registration.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"width:25%\">\n  <h2>Registration</h2>\n\n  <form #userForm=\"ngForm\">\n    <div class=\"form-group\">\n      <label for=\"account\">Account Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"account\" required\n             [(ngModel)]=\"model.accountName\" name=\"account\"\n             pattern=\"^[a-zA-Z][a-zA-Z0-9]+$\"\n             #account=\"ngModel\">\n      <div [hidden]=\"account.valid || account.pristine\"\n           class=\"alert alert-danger\">\n        Account Name is required and it can only start with letter.\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"display\">Display Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"display\"\n             [(ngModel)]=\"model.display\" name=\"display\">\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"email\">Email</label>\n      <input type=\"text\" class=\"form-control\" id=\"email\" required\n             [(ngModel)]=\"model.email\" name=\"email\"\n             pattern=\"^[a-zA-Z0–9_.+-]+@[a-zA-Z0–9-]+.[a-zA-Z0–9.-]+$\"\n             #email=\"ngModel\">\n      <div [hidden]=\"email.valid || email.pristine\"\n           class=\"alert alert-danger\">\n        Email is required and format should be <i>john@domain.com</i>.\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"phone\">Phone</label>\n      <input type=\"text\" class=\"form-control\" id=\"phone\" required\n             [(ngModel)]=\"model.phoneNumber\" name=\"phone\"\n             pattern=\"^\\d\\d\\d-\\d\\d\\d-\\d\\d\\d\\d$\"\n             #phone=\"ngModel\">\n      <div [hidden]=\"phone.valid || phone.pristine\"\n           class=\"alert alert-danger\">\n        Phone Number is required and format should be <i>123-456-7890</i>.\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"birthday\">Date of Birth</label>\n      <input type=\"date\" class=\"form-control\" id=\"birthday\" required\n             [(ngModel)]=\"model.dateOfBirth\" name=\"birthday\"\n             #birthday=\"ngModel\" validateAge=\"birthday\">\n      <div [hidden]=\"birthday.valid || birthday.pristine\"\n           class=\"alert alert-danger\">\n        Sorry, your age is under 18\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label for=\"zip\">Zip Code</label>\n      <input type=\"number\" class=\"form-control\" id=\"zip\" required\n             [(ngModel)]=\"model.zip\" name=\"zip\"\n             pattern=\"^\\d\\d\\d\\d\\d$\"\n             #zip=\"ngModel\">\n      <div [hidden]=\"zip.valid || zip.pristine\"\n           class=\"alert alert-danger\">\n        Zip Code is required and consists of five integers\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label>Password</label>\n      <input type=\"password\" class=\"form-control\" name=\"password\" [ngModel]=\"model.psw\"\n             required #password=\"ngModel\" validateEqual=\"confirmPassword\" reverse=\"true\">\n      <div [hidden]=\"password.valid || (password.pristine)\">\n        Password is required\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label>Retype password</label>\n      <input type=\"password\" class=\"form-control\" name=\"confirmPassword\" [ngModel]=\"model.repsw\"\n             required validateEqual=\"password\" #confirmPassword=\"ngModel\">\n      <div [hidden]=\"confirmPassword.valid ||  (confirmPassword.pristine)\">\n        Password mismatch\n      </div>\n    </div>\n\n    <button type=\"submit\" class=\"btn btn-success\"\n            [disabled]=\"!userForm.form.valid\"\n            (click)=\"register()\"\n            routerLink=\"/login\" routerLinkActive=\"active\">\n      Submit\n    </button>\n    <button type=\"button\" class=\"btn btn-default\"\n            (click)=\"userForm.reset()\">\n      Reset\n    </button>\n\n  </form>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/registration/registration.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__("../../../../../src/app/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_service__ = __webpack_require__("../../../../../src/app/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegistrationComponent = (function () {
    function RegistrationComponent(dataService) {
        this.dataService = dataService;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](0, '', '', '', '', '', '', '', '', '', [], [], true, '');
        this.submitted = false;
        this.id = 0;
    }
    RegistrationComponent.prototype.ngOnInit = function () {
        this.submitted = true;
    };
    RegistrationComponent.prototype.register = function () {
        var _this = this;
        this.dataService.getUsers()
            .then(function (users) {
            _this.id = users[0].id + 1;
            return users;
        })
            .then(function (users) {
            _this.model = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */](_this.id, '', '', '', '', '', '', '', '', '', [], [], true, '');
            return users;
        })
            .then(function (users) { return users.unshift(_this.model); });
    };
    return RegistrationComponent;
}());
RegistrationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-registration',
        template: __webpack_require__("../../../../../src/app/registration/registration.component.html"),
        styles: [__webpack_require__("../../../../../src/app/registration/registration.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__data_service__["a" /* DataService */]) === "function" && _a || Object])
], RegistrationComponent);

var _a;
//# sourceMappingURL=registration.component.js.map

/***/ }),

/***/ "../../../../../src/app/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(id, accountName, phoneNumber, zip, email, psw, repsw, dateOfBirth, status, img, following, articles, display, displayName) {
        this.id = id;
        this.accountName = accountName;
        this.phoneNumber = phoneNumber;
        this.zip = zip;
        this.email = email;
        this.psw = psw;
        this.repsw = repsw;
        this.dateOfBirth = dateOfBirth;
        this.status = status;
        this.img = img;
        this.following = following;
        this.articles = articles;
        this.display = display;
        this.displayName = displayName;
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ "../../../../../src/app/validator/age-validator.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgeValidator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AgeValidator = AgeValidator_1 = (function () {
    function AgeValidator(validateEqual) {
        this.validateEqual = validateEqual;
    }
    AgeValidator.prototype.validate = function (c) {
        // self value
        if (c.value == null) {
            return {
                validateEqual: false
            };
        }
        var v = parseInt(c.value.slice(0, 4), 10);
        if (new Date().getFullYear() - v < 18) {
            return {
                validateEqual: false
            };
        }
        return null;
    };
    return AgeValidator;
}());
AgeValidator = AgeValidator_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* Directive */])({
        selector: '[validateAge][formControlName],[validateAge][formControl],[validateAge][ngModel]',
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* NG_VALIDATORS */], useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_24" /* forwardRef */])(function () { return AgeValidator_1; }), multi: true }
        ]
    }),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* Attribute */])('validateAge')),
    __metadata("design:paramtypes", [String])
], AgeValidator);

var AgeValidator_1;
//# sourceMappingURL=age-validator.directive.js.map

/***/ }),

/***/ "../../../../../src/app/validator/equal-validator.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EqualValidator; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var EqualValidator = EqualValidator_1 = (function () {
    function EqualValidator(validateEqual, reverse) {
        this.validateEqual = validateEqual;
        this.reverse = reverse;
    }
    Object.defineProperty(EqualValidator.prototype, "isReverse", {
        get: function () {
            if (!this.reverse)
                return false;
            return this.reverse === 'true' ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    EqualValidator.prototype.validate = function (c) {
        // self value
        var v = c.value;
        // control vlaue
        var e = c.root.get(this.validateEqual);
        // value not equal
        if (e && v !== e.value && !this.isReverse) {
            return {
                validateEqual: false
            };
        }
        // value equal and reverse
        if (e && v === e.value && this.isReverse) {
            delete e.errors['validateEqual'];
            if (!Object.keys(e.errors).length)
                e.setErrors(null);
        }
        // value not equal and reverse
        if (e && v !== e.value && this.isReverse) {
            e.setErrors({ validateEqual: false });
        }
        return null;
    };
    return EqualValidator;
}());
EqualValidator = EqualValidator_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* Directive */])({
        selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* NG_VALIDATORS */], useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_24" /* forwardRef */])(function () { return EqualValidator_1; }), multi: true }
        ]
    }),
    __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* Attribute */])('validateEqual')),
    __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["h" /* Attribute */])('reverse')),
    __metadata("design:paramtypes", [String, String])
], EqualValidator);

var EqualValidator_1;
//# sourceMappingURL=equal-validator.directive.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map