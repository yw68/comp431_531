const express = require('express');
const router = express.Router();
const model = require('./model');
const User = require('./model').User;
const md5 = require('md5');
const bcrypt = require('bcryptjs');
const cookieKey = 'sid';
const redis = require("redis"),
    client = redis.createClient();

router.post('/login', function (req, res, next) {
    const username = req.body.accountName;
    const password = req.body.psw;
    if (!username || !password) {
        res.sendStatus(400);
        return
    }
    // build sessionKey
    const sessionKey = md5('sH^&]4d|*(#@`~9' + new Date().getTime() + username);
    // set cookie
    User.findOne({accountName: username}, function (err, user) {
        if (err) return console.log(err);
        else {
            if (!user || user.length === 0) {
                res.status(401).send({username: username, result: 'invalid user'})
                return
            }
            if (bcrypt.compareSync(password, user.psw)) {
                client.set(sessionKey, username);
                res.cookie(cookieKey, sessionKey, {maxAge: 3600 * 1000, httpOnly: true});
                res.status(200).send({username: username, result: 'Success'})
            } else {
                res.status(401).send({username: username, result: 'incorrect password'})
            }
        }
    })
});


router.post('/register', function (req, res, next) {
    const accountName = req.body.accountName;
    const displayName = req.body.displayName;
    const zip = req.body.zip;
    const dob = req.body.dob;
    const avatar = req.body.avatar;
    const email = req.body.email;
    const phoneNumber = req.body.phoneNumber;
    const headline = req.body.headline;
    const psw = req.body.psw;

    const newUser = new User({
        accountName: accountName,
        displayName: displayName,
        zip: zip,
        dob: dob,
        avatar: avatar,
        email: email,
        phoneNumber: phoneNumber,
        headline: headline,
        psw: psw
    });
    model.createUser(newUser, function (err, user) {
        if (err) throw err;
        console.log(user);
        res.status(200).send({result: 'success', username: accountName})
    });
});